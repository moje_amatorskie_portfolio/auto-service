<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        if ($user->hasRole('workshop owner')) {
            $currentUserWorkshop = $user->workshop()->firstOrFail();
            $workshopVehicles = $currentUserWorkshop->vehicles()->get();
            $vehicleOwners = collect(); //empty collection
            foreach ($workshopVehicles as $workshopVehicle) {
                $vehicleOwners = $vehicleOwners->concat($workshopVehicle->users()->get()->whereNotIn('id', $vehicleOwners->pluck('id'))); //prevents duplicates
            }
            if (!$vehicleOwners->contains('id', $model->id)) {
                return Response::deny('You cannot delete vehicle owner that does not belong to your workshop.');
            }
            return Response::allow();
        }
    }
}
