<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;

class VehiclePolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Vehicle  $vehicle
     * @return mixed
     */
    public function view(User $user, Vehicle $vehicle)
    {
        if ($user->hasRole('workshop owner')) {
            $workshopVehicles = $user->workshop()->firstOrFail()->vehicles()->get();
            return $workshopVehicles->contains('id', $vehicle->id) ? Response::allow() : Response::deny("Sorry, this vehicle is not registered with your workshop.");
        }

        return
            (
                $vehicle->users()->get()->contains('id', $user->id)
                ||
                $user->sharedVehicles()->get()->contains('id', $vehicle->id)
            )
            ? Response::allow()
            : Response::deny("Sorry, the owner of this vehicle did not let you see their vehicle");
    }

    /**
     * Determine whether the user can edit the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Vehicle  $vehicle
     * @return mixed
     */
    public function edit(User $user, Vehicle $vehicle)
    {
        return (
            $vehicle->users()->get()->contains('id', $user->id)
        )
            ? Response::allow()
            : Response::deny("Sorry, you can't edit a vehicle that is not yours...");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Vehicle  $vehicle
     * @return mixed
     */
    public function update(User $user, Vehicle $vehicle)
    {
        return (
            $vehicle->users()->get()->contains('id', $user->id)
        )
            ? Response::allow()
            : Response::deny("Sorry, you can't update a vehicle that is not yours...");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Vehicle  $vehicle
     * @return mixed
     */
    public function delete(User $user, Vehicle $vehicle)
    {
        if ($user->hasRole('vehicle owner')) {
            return (
            $vehicle->users()->get()->contains('id', $user->id)
        )
            ? Response::allow()
            : Response::deny("Sorry, you can't delete a vehicle that is not yours...");
        }
        if ($user->hasRole('workshop owner')) {
            $workshopVehicles = $user->workshop()->firstOrFail()->vehicles()->get();
            return $workshopVehicles->contains('id', $vehicle->id) ? Response::allow() : Response::deny("Sorry, you can't delete a vehicle that is not registered with your workshop.");
        }
    }
}
