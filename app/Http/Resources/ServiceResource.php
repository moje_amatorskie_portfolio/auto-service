<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Vehicle;
use App\Models\User;
use App\Models\Workshop;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $workshop = Workshop::find($this->workshop_id);

        return [
            'id' => $this->id,
            'title' => $this->title,
            'owner' => Vehicle::find($this->vehicle_id)->users()->firstOrFail()->full_name,
            'description' => $this->description,
            'start' => $this->start,
            'end' => $this->end,
            'place' => $workshop->place,
            'street' => $workshop->street,
            'longitude' =>  $workshop->longitude,
            'latitude' => $workshop->latitude,
            'make' => Vehicle::find($this->vehicle_id)->make,
            'model' => Vehicle::find($this->vehicle_id)->model,
            'is_approved' => $this->is_approved,
            'workshop' => $workshop->name,
        ];
    }
}
