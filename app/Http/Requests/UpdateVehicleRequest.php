<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update vehicle');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'make' => 'required|string',
            'model' => 'required|string',
            'fuel' => 'required|in:petrol,diesel',
            'capacity' => 'required|integer',
            'power' => 'required|integer',
            'vin' => 'required|regex:/^[\d]{1}[A-Z]{4}[\d]{2}[A-Z]{4}[\d]{6}$/',
            'plate' => 'required|string|regex:/^[A-Z]{2,8}\s[\d]{3,7}$/',
            'equipment' => 'required|string',
            'description' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'vin.regex' => 'VIN should have a different format, e.g. 1HGBH41JXMN109186',
            'plate.regex' => 'Plate should have a different format, e.g. BFR 58674',
        ];
    }
}
