<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        Gate::authorize('show dashboard');

        return redirect()->route('vehicles.index');
    }
}
