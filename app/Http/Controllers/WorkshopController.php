<?php

namespace App\Http\Controllers;

use App\Models\VehicleOwner;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Vehicle;
use App\Models\Workshop;
use App\Http\Requests\StoreWorkshopRequest;
use App\Http\Requests\UpdateWorkshopRequest;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Ulluminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Services\StringGeneratorService;

class WorkshopController extends Controller
{
    private $userRepository;
    private $stringGeneratorService;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->stringGeneratorService = new StringGeneratorService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('index workshops');

        return view('workshops.index');
    }

    public function index_api()
    {
        Gate::authorize('index workshops');
        $workshops = Workshop::all();

        return $workshops;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('create workshop');
        $formHeader = 'Add workshop';
        $formType = 'create';
        $route = route('workshops.store');
        return view('workshops.form', compact('formHeader', 'formType', 'route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWorkshopRequest $request)
    {
        Gate::authorize('store workshop');
        $workshop = new Workshop();
        $workshop->name = $request->name;
        $workshop->place = $request->place;
        $workshop->street = $request->street;
        $workshop->longitude = $request->longitude;
        $workshop->latitude = $request->latitude;
        $workshop->save();

        $workshopOwner = new User();
        $workshopOwner->first_name = "F.N. of ".$workshop->name;
        $workshopOwner->last_name = "L.N. of ".$workshop->last_name;
        $workshopOwner->email = $this->stringGeneratorService->generate_string('abcd', 3).'@'.$this->stringGeneratorService->generate_string('efgh', 3).'.com';
        $workshopOwner->address = 'Address';
        $workshopOwner->password = Hash::make('password');
        $workshopOwner->assignRole('workshop owner');
        $workshopOwner->workshop_id = $workshop->id;
        $workshopOwner->save();

        return view('workshops.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Workshop $workshop)
    {
        Gate::authorize('show workshop');
        $formHeader = 'View workshop';
        $formType = 'show';
        $disabled = 'disabled';
        $route = route('workshops.index');

        return view('workshops.form', compact('formHeader', 'formType', 'disabled', 'workshop', 'route'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Workshop $workshop)
    {
        Gate::authorize('edit workshop');
        $formHeader = 'Edit workshop';
        $formType = 'edit';
        $route = route('workshops.update', $workshop);

        return view('workshops.form', compact('formHeader', 'formType', 'route', 'workshop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWorkshopRequest $request, Workshop $workshop)
    {
        $this->authorize('update workshop');
        $workshop->name = $request->name;
        $workshop->place = $request->place;
        $workshop->street = $request->street;
        $workshop->longitude = $request->longitude;
        $workshop->latitude = $request->latitude;
        $workshop->save();

        return redirect()->route('workshops.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workshop $workshop)
    {
        Gate::authorize('destroy workshop');
        $workshop->delete();
    }
}
