<?php

namespace App\Http\Controllers;

use App\Models\VehicleOwner;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Vehicle;
use App\Http\Requests\StoreVehicleOwnerRequest;
use App\Http\Requests\UpdateVehicleRequest;
use App\Http\Requests\UpdateVehicleOwnerRequest;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use App\Services\VinGeneratorService;
use App\Services\VehicleGeneratorService;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Ulluminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class VehicleOwnerController extends Controller
{
    private $userRepository;
    private $vinGeneratorService;
    private $vehicleGeneratorService;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->vinGeneratorService = new VinGeneratorService();
        $this->vehicleGeneratorService = new VehicleGeneratorService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('index vehicle owners');
        return view('vehicle_owners.index');
    }

    public function index_api()
    {
        Gate::authorize('index vehicle owners');
        if (Auth::user()->hasRole('super admin')) {
            $vehicleOwners = $this->userRepository->getVehicleOwners();
        }
        if (Auth::user()->hasRole('workshop owner')) {
            $currentUserWorkshop = Auth::user()->workshop()->firstOrFail();
            $workshopVehicles = $currentUserWorkshop->vehicles()->get();
            $vehicleOwners = collect();
            foreach ($workshopVehicles as $workshopVehicle) {
                $vehicleOwners = $vehicleOwners->concat($workshopVehicle->users()->get()->whereNotIn('id', $vehicleOwners->pluck('id'))); //prevents duplicates
            }
        }
        return $vehicleOwners;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('create vehicle owner');
        $formHeader = 'Add vehicle owner';
        $formType = 'create';
        $route = route('vehicle-owners.store');

        return view('vehicle_owners.form', compact('formHeader', 'formType', 'route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVehicleOwnerRequest $request)
    {
        Gate::authorize('store vehicle owner');
        $vehicleOwner = new User();
        $vehicleOwner->first_name = $request->first_name;
        $vehicleOwner->last_name = $request->last_name;
        $vehicleOwner->email = $request->email;
        $vehicleOwner->address = $request->address;
        $vehicleOwner->password = Hash::make($request->password);
        $vehicleOwner->save();
        $vehicleOwner->assignRole('vehicle owner');

        return redirect()->route('vehicle-owners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(User $vehicleOwner)
    {
        Gate::authorize('show vehicle owner');
        $formHeader = 'View vehicle owner';
        $formType = 'show';
        $disabled = 'disabled';
        $route = route('vehicle-owners.index');

        return view('vehicle_owners.form', compact('formHeader', 'formType', 'disabled', 'vehicleOwner', 'route'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(User $vehicleOwner)
    {
        Gate::authorize('edit vehicle owner');
        $formHeader = 'Edit vehicle owner';
        $formType = 'edit';
        $route = route('vehicle-owners.update', $vehicleOwner);

        return view('vehicle_owners.form', compact('formHeader', 'formType', 'route', 'vehicleOwner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVehicleOwnerRequest $request, User $vehicleOwner)
    {
        $this->authorize('update vehicle owner');
        $vehicleOwner->first_name = $request->first_name;
        $vehicleOwner->last_name = $request->last_name;
        $vehicleOwner->email = $request->email;
        $vehicleOwner->address = $request->address;
        if (!empty($request->password)) {
            $vehicleOwner->password = Hash::make($request->password);
        }
        $vehicleOwner->save();

        return redirect()->route('vehicle-owners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $vehicleOwner)
    {
        Gate::authorize('destroy vehicle owner');
        $this->authorize('delete', $vehicleOwner);
        $loggedInUser = Auth::user();
        if ($loggedInUser->hasRole('workshop owner')) {
            $workshopId = $loggedInUser->workshop_id;
            $workshopVehiclesIds = $loggedInUser->workshop()->firstOrFail()->vehicles()->get()->pluck('id');
            $vehiclesOfVehicleOwnerToDelete = $vehicleOwner->vehicles()->get();
            foreach ($vehiclesOfVehicleOwnerToDelete as $vehicleOfVehicleOwnerToDelete) {
                if ($workshopVehiclesIds->contains($vehicleOfVehicleOwnerToDelete->id)) {
                    $vehicleOfVehicleOwnerToDelete->workshops()->sync([7]);
                }// inserts workshop named 'none' into intermediate table "vehicle workshop"
            }
        } else {
            //super admin
            $vehiclesToDelete = $vehicleOwner->vehicles()->get();
            $vehicleOwner->delete();
            foreach ($vehiclesToDelete as $vehicleToDelete) {
                $vehicleToDelete->delete();
            }
        }
    }

    public function generateVehicle()
    {
        return $generatedVehicle = $this->vehicleGeneratorService->vehicleGeneratorService();
    }
}
