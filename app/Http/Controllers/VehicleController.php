<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use App\Models\Workshop;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreVehicleRequest;
use App\Http\Requests\UpdateVehicleRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use App\Services\VinGeneratorService;
use App\Services\VehicleGeneratorService;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Ulluminate\Support\Facades\File;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Redirect;
use PDF;

// use Illuminate\Validation\Validator;

class VehicleController extends Controller
{
    private $userRepository;
    private $vinGeneratorService;
    private $vehicleGeneratorService;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->vinGeneratorService = new VinGeneratorService();
        $this->vehicleGeneratorService = new VehicleGeneratorService();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        Gate::authorize('index vehicles');
        $ownVehicles = Auth::user()->vehicles()->get();
        return
            empty($request->bearerToken()) //this is to check if this is a request from a mobile app or from a desktop app
            ? view('vehicles.index', compact('ownVehicles'))
            : Vehicle::all()->toJson();
    }

    public function index_api()
    {
        Gate::authorize('index vehicles');
        if (Auth::user()->hasRole('super admin')) {
            $vehicles = Vehicle::all();
            return $vehicles;
        }
        if (Auth::user()->hasRole('vehicle owner')) {
            $ownVehicles = Auth::user()->vehicles()->get();
            $vehicles = $ownVehicles->concat(Auth::user()->sharedVehicles()->get()); //add vehicles that other users allowed current user to view
            return $vehicles;
        }
        if (Auth::user()->hasRole('workshop owner')) {
            $currentUserWorkshop = Auth::user()->workshop()->firstOrFail();
            $workshopVehicles = $currentUserWorkshop->vehicles()->get();
            return $workshopVehicles;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('create vehicle');
        $formHeader = 'Add vehicle';
        $formType = 'create';
        $route = route('vehicles.store');
        $generatedVehicle = $this->vehicleGeneratorService->vehicleGeneratorService();
        $make = $generatedVehicle['make'];
        $model = $generatedVehicle['model'];
        $fuel = $generatedVehicle['fuel'];
        $capacity = $generatedVehicle['capacity'];
        $power = $generatedVehicle['power'];
        $vin = $generatedVehicle['vin'];
        $plate = $generatedVehicle['plate'];
        $equipment = $generatedVehicle['equipment'];
        $description = $generatedVehicle['description'];
        $vehicleOwners = $this->userRepository->getVehicleOwners()->except(Auth::id()); //vehicle owners without current user
        $workshops = Workshop::all();

        return view('vehicles.create', compact('formHeader', 'formType', 'make', 'model', 'fuel', 'capacity', 'power', 'vin', 'plate', 'equipment', 'description', 'route', 'vehicleOwners', 'workshops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVehicleRequest $request)
    {
        Gate::authorize('store vehicle');
        $vehicle = new Vehicle();
        $vehicle->make = $request->make;
        $vehicle->model = $request->model;
        $vehicle->fuel = $request->fuel;
        $vehicle->capacity = $request->capacity;
        $vehicle->power = $request->power;
        $vehicle->vin = $request->vin;
        $vehicle->plate = $request->plate;
        $vehicle->equipment = $request->equipment;
        $vehicle->description = $request->description;
        $vehicle->save();
        $vehicle->sharedWithUsers()->sync($request->shared_with_users);
        foreach ($request->input('document', []) as $file) {
            $mediaItem = $vehicle->addMedia(public_path('images/vehicles/' . $file))->preservingOriginal()->toMediaCollection('document');
            $mediaItem->setCustomProperty('id', $mediaItem->id);
            $mediaItem->save();
        }

        DB::table('user_vehicle')->insert([
            'user_id' => Auth::user()->id,
            'vehicle_id' => $vehicle->id,
        ]);
        DB::table('vehicle_workshop')->insert([
        'vehicle_id' => $vehicle->id,
        'workshop_id' => $request->workshop,
        ]);

        return
            empty($request->bearerToken())
            ? Redirect::route('vehicles.index')
            : Vehicle::all()->toJson();
    }

    public function storeMedia(Request $request)
    {
        $path = public_path('images/vehicles');
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $image = $request->file('file');
        $original = $image->getClientOriginalName();
        $basename = Str::random();
        $name = $basename.'.'.$image->getClientOriginalExtension();
        $image->move(public_path('/images/vehicles/'), $name);

        return response()->json([
            'original_name' => $name,
            'name'          => $name,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Vehicle $vehicle)
    {
        Gate::authorize('show vehicle');
        $this->authorize('view', $vehicle);
        $formHeader = 'View vehicle';
        $formType = 'show';
        $disabled = 'disabled';
        $vehicle->document = $vehicle->getMedia('document');
        $route = route('vehicles.index');
        $vehicleOwners = $this->userRepository->getVehicleOwners()->except(Auth::id());
        $workshops = Workshop::all();

        return
            empty($request->bearerToken())
            ? view('vehicles.create', compact('formHeader', 'formType', 'disabled', 'vehicle', 'route', 'vehicleOwners', 'workshops'))
            : $vehicle->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vehicle $vehicle)
    {
        Gate::authorize('edit vehicle');
        $this->authorize('edit', $vehicle);
        $formHeader = 'Edit vehicle';
        $formType = 'edit';
        $route = route('vehicles.update', $vehicle);
        $workshops = Workshop::all();
        $vehicle->document = $vehicle->getMedia('document');
        $vehicleOwners = $this->userRepository->getVehicleOwners()->except(Auth::id());
        return
            empty($request->bearerToken())
            ? view('vehicles.create', compact('formHeader', 'formType', 'route', 'vehicle', 'vehicleOwners', 'workshops'))
            : $vehicle->toJson(JSON_PRETTY_PRINT);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVehicleRequest $request, Vehicle $vehicle)
    {
        Gate::authorize('update vehicle');
        $this->authorize('update', $vehicle);
        $vehicle->make = $request->make;
        $vehicle->model = $request->model;
        $vehicle->fuel = $request->fuel;
        $vehicle->capacity = $request->capacity;
        $vehicle->power = $request->power;
        $vehicle->vin = $request->vin;
        $vehicle->plate = $request->plate;
        $vehicle->equipment = $request->equipment;
        $vehicle->description = $request->description;
        $vehicle->save();
        if (!empty($request->sharedWithUsers)) {
            $vehicle->sharedWithUsers()->sync($request->shared_with_users);
        }
        if (!empty($request->workshop)) {
            $vehicle->workshops()->sync($request->workshop);
        }

        $mediaFromDB = $vehicle->getMedia('document');
        if (count($mediaFromDB) > 0) {
            foreach ($mediaFromDB as $medium) {
                if (!in_array($medium->file_name, $request->input('document', []))) {
                    $medium->delete();
                }
            }
        }

        $media = $mediaFromDB->pluck('file_name')->toArray();
        foreach ($request->input('document', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $mediaItem = $vehicle->addMedia(public_path('images/vehicles/' . $file))->preservingOriginal()->toMediaCollection('document');
                $mediaItem->setCustomProperty('id', $mediaItem->id);
                $mediaItem->save();
            }
        }

        return
            empty($request->bearerToken())
            ? redirect()->route('vehicles.index')
            : Vehicle::all()->toJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        Gate::authorize('destroy vehicle');
        $this->authorize('delete', $vehicle);
        $user = Auth::user();
        if ($user->hasRole('workshop owner')) {
            $user->workshop()->firstOrFail()->vehicles()->detach($vehicle->id);
            $vehicle->workshops()->attach(7);// inserts workshop named 'none' into intermediate table "vehicle workshop"
        } else {
            $vehicle->delete();
        }
    }

    public function indexVehicleOwners()
    {
        Gate::authorize('index vehicle owners');
        $vehicleOwners = $this->userRepository->getVehicleOwners();

        return view('vehicle_owners.index', compact('vehicleOwners'));
    }

    public function generateVehicle()
    {
        return $generatedVehicle = $this->vehicleGeneratorService->vehicleGeneratorService();
    }

    public function listVehiclesInPdf()
    {
        Gate::authorize('index vehicles');
        if (Auth::user()->hasRole('super admin')) {
            $vehicles = Vehicle::all();
        } elseif (Auth::user()->hasRole('vehicle owner')) {
            $ownVehicles = Auth::user()->vehicles()->get();
            $vehicles = $ownVehicles->concat(Auth::user()->sharedVehicles()->get());
        } elseif (Auth::user()->hasRole('workshop owner')) {
            $currentUserWorkshop = Auth::user()->workshop()->firstOrFail();
            $vehicles = $currentUserWorkshop->vehicles()->get();
        } else {
            abort(403);
        }
        $pdf = PDF::loadView('vehicles.pdf', ['vehicles' => $vehicles]);

        return $pdf->download('vehicles.pdf');
    }
}
