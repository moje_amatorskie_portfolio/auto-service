<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\ServiceResource;
use App\Repositories\ServiceRepository;

class MapController extends Controller
{
    private $serviceRepository;

    public function __construct()
    {
        $this->serviceRepository = new ServiceRepository();
    }

    public function index()
    {
        Gate::authorize('index map');
        return view('map.index');
    }
}
