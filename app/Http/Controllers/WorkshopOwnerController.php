<?php

namespace App\Http\Controllers;

use App\Models\WorkshopOwner;
use App\Models\VehicleOwner;
use App\Models\User;
use App\Models\Workshop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Vehicle;
use App\Http\Requests\StoreWorkshopOwnerRequest;
use App\Http\Requests\UpdateWorkshopOwnerRequest;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Ulluminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Database\Seeder;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

// use Illuminate\Validation\Validator;

class WorkshopOwnerController extends Controller
{
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('index workshop owners');

        return view('workshop_owners.index');
    }

    public function index_api()
    {
        Gate::authorize('index workshop owners');
        $workshopOwners = $this->userRepository->getWorkshopOwners();
        return $workshopOwners;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('create workshop owner');
        $formHeader = 'Add workshop owner';
        $formType = 'create';
        $route = route('workshop-owners.store');
        return view('workshop_owners.form', compact('formHeader', 'formType', 'route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWorkshopOwnerRequest $request)
    {
        Gate::authorize('store workshop owner');
        $workshop = new Workshop();

        $workshopOwner = new User();
        $workshopOwner->first_name = $request->first_name;

        $workshop->name = $workshopOwner->first_name."'s workshop";
        $workshop->save();

        $workshopOwner->last_name = $request->last_name;
        $workshopOwner->email = $request->email;
        $workshopOwner->address = $request->address;
        $workshopOwner->password = Hash::make($request->password);
        $workshopOwner->assignRole('workshop owner');
        $workshopOwner->workshop_id = $workshop->id;
        $workshopOwner->save();

        return view('workshop_owners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(User $workshopOwner)
    {
        Gate::authorize('show workshop owner');
        $formHeader = 'View workshop owner';
        $formType = 'show';
        $disabled = 'disabled';
        $route = route('workshop-owners.index');

        return view('workshop_owners.form', compact('formHeader', 'formType', 'disabled', 'workshopOwner', 'route'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(User $workshopOwner)
    {
        Gate::authorize('edit workshop owner');
        $formHeader = 'Edit workshop owner';
        $formType = 'edit';
        $route = route('workshop-owners.update', $workshopOwner);

        return view('workshop_owners.form', compact('formHeader', 'formType', 'route', 'workshopOwner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWorkshopOwnerRequest $request, User $workshopOwner)
    {
        $this->authorize('update workshop owner');
        $workshopOwner->first_name = $request->first_name;
        $workshopOwner->last_name = $request->last_name;
        $workshopOwner->email = $request->email;
        $workshopOwner->address = $request->address;
        if (!empty($request->password)) {
            $workshopOwner->password = Hash::make($request->password);
        }
        $workshopOwner->save();

        return redirect()->route('workshop-owners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $workshopOwner)
    {
        Gate::authorize('destroy workshop owner');
        $workshopOwner->delete();
    }
}
