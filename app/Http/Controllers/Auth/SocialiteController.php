<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Workshop;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SocialiteController extends Controller
{
    public function loginWithGoogle()
    {
        return Socialite::driver('google')->scopes(['email','profile'])->redirect();
    }

    public function loginWithFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callbackFromGoogle(Request $request)
    {
        try {
            $socialiteUser = Socialite::driver('google')->user();
            $socialiteUser->user['first_name'] = $socialiteUser->user['given_name'];
            $socialiteUser->user['last_name'] = $socialiteUser->user['family_name'];
            $is_user = User::where('email', $socialiteUser->getEmail())->first();
            if (!$is_user) {
                return view('auth.choose-user-type', ['socialiteUser' => $socialiteUser]);
            } else {
                $socialiteUser = User::where('email', $socialiteUser->getEmail())->first();
            }
            Auth::loginUsingId($socialiteUser->id);
            $request->session()->regenerate();

            return redirect()->intended(RouteServiceProvider::HOME);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function callbackFromFacebook(Request $request)
    {
        try {
            $socialiteUser = Socialite::driver('facebook')->user();
            $nameArray = explode(" ", $socialiteUser->getName());
            $socialiteUser->user['first_name'] = $nameArray[0];
            $socialiteUser->user['last_name'] = $nameArray[1];
            $is_user = User::where('email', $socialiteUser->getEmail())->first();
            if (!$is_user) {
                return view('auth.choose-user-type', ['socialiteUser' => $socialiteUser]);
            } else {
                $socialiteUser = User::where('email', $socialiteUser->getEmail())->first();
            }
            Auth::loginUsingId($socialiteUser->id);
            $request->session()->regenerate();

            return redirect()->intended(RouteServiceProvider::HOME);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function storeUserType(Request $request)
    {
        $newUser = new User();
        $newUser->first_name = $request->first_name;
        $newUser->last_name = $request->last_name;
        $newUser->email = $request->email;

        if ($request->workshop_owner) {
            $workshop = new Workshop();
            $workshop->name = $newUser->first_name."'s workshop";
            $workshop->save();
            $newUser->workshop_id = $workshop->id;
            $newUser->assignRole('workshop owner');
        } else {
            $newUser->assignRole('vehicle owner');
        }
        $newUser->save();
        Auth::loginUsingId($newUser->id);
        $request->session()->regenerate();

        return redirect()->intended(RouteServiceProvider::HOME);
    }
}
