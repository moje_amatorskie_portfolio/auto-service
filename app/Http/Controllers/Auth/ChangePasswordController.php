<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\ChangePasswordRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function show_form()
    {
        $formHeader = 'Change password';
        $route = route('vehicles.index');
        return view('auth.passwords.change', ['formHeader' => $formHeader, 'route' => $route]);
    }

    public function change(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->password);
        $user->save();
        $message = 'The password has been changed';

        return redirect()->route('vehicles.index')->with('message', $message);
    }
}
