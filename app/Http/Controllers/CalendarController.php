<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Models\Workshop;
use App\Models\User;
use App\Models\Service;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use App\Repositories\UserRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Ulluminate\Support\Facades\File;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Redirect;
use App\Http\Resources\ServiceResource;
use App\Repositories\VehicleRepository;
use App\Repositories\ServiceRepository;
use PDF;

class CalendarController extends Controller
{
    private $vehicleRepository;
    private $serviceRepository;

    public function __construct()
    {
        $this->vehicleRepository = new VehicleRepository();
        $this->serviceRepository = new ServiceRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('index services');
        if (Auth::user()->hasRole('vehicle owner')) {
            $ownVehicles = $this->vehicleRepository->getOwnVehicles();
            return view('calendar.index', ['ownVehicles' => $ownVehicles]);
        }
        return view('calendar.index', ['disabled' => 'disabled']);
    }

    public function index_api($isApproved)
    {
        Gate::authorize('index services');
        $currentUser = Auth::user();
        if ($currentUser->hasRole('super admin')) {
            $services = Service::all()->where('is_approved', $isApproved);
        }
        if ($currentUser->hasRole('vehicle owner')) {
            $services = $this->serviceRepository->getVehicleOwnerServices($currentUser)->where('is_approved', $isApproved);
        }
        if ($currentUser->hasRole('workshop owner')) {
            $services = $this->serviceRepository->getWorkshopOwnerServices($currentUser)->where('is_approved', $isApproved);
        }
        return ServiceResource::collection($services);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Gate::authorize('store service');
        $service  =  new Service();
        $service->title = $request->title;
        $service->description = $request->description;
        $service_day = $request->service_day;
        $service->service_day = $service_day;
        $service->start = $service_day." ".$request->start.":00:00";
        $service->end = $service_day." ".$request->end.":00:00";
        $service->vehicle_id = $request->vehicle_id;
        $vehicle  =  Vehicle::find($request->vehicle_id);
        $service->workshop_id = $vehicle->workshops()->firstOrFail()->id;
        $service->save();

        return redirect()->route('calendar.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('update service');
        $service = Service::find($id);
        $service->is_approved = empty($request->isApproved) ? 0 : $request->isApproved;
        $service->save();
        return redirect()->route('calendar.index');
    }
}
