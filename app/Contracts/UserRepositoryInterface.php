<?php

namespace App\Contracts;

interface UserRepositoryInterface
{
    public function getVehicleOwners();
}
