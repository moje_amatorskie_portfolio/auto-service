<?php

namespace App\Contracts;

interface ServiceRepositoryInterface
{
    public function getVehicleOwnerServices($vehicleOwner);
}
