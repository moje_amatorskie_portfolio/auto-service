<?php

namespace App\Contracts;

interface VehicleRepositoryInterface
{
    public function getOwnVehicles();
}
