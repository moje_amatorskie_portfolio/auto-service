<?php

namespace App\Repositories;

use App\Models\Vehicle;
use App\Models\User;
use App\Models\Service;
use App\Contracts\ServiceRepositoryInterface;
use App\Http\Resources\ServiceResource;
use App\Repositories\BaseRepository;
use App\Repositories\VehicleRepository;
use Illuminate\Support\Facades\Auth;

class ServiceRepository extends BaseRepository implements ServiceRepositoryInterface
{
    private $vehicleRepository;

    public function __construct()
    {
        $this->vehicleRepository = new VehicleRepository();
    }
    public function getVehicleOwnerServices($vehicleOwner)
    {
        $ownVehicles = $this->vehicleRepository->getOwnVehicles();
        $services = collect();
        foreach ($ownVehicles as $ownVehicle) {
            $services = $services->merge($ownVehicle->services);
        }
        return $services;
    }
    public function getWorkshopOwnerServices($workshopOwner)
    {
        $services = $workshopOwner->workshop()->firstOrFail()->services()->get();
        return $services;
    }

    public function getServicesForCurrentUser()
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('super admin')) {
            $services = Service::all();
        }
        if ($currentUser->hasRole('vehicle owner')) {
            $services = $this->getVehicleOwnerServices($currentUser);
        }
        if ($currentUser->hasRole('workshop owner')) {
            $services = $this->getWorkshopOwnerServices($currentUser);
        }
        return ServiceResource::collection($services);
    }
}
