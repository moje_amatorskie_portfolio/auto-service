<?php

namespace App\Repositories;

use App\Models\Vehicle;
use App\Contracts\VehicleRepositoryInterface;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

class VehicleRepository extends BaseRepository implements VehicleRepositoryInterface
{
    public function getOwnVehicles()
    {
        $user = Auth::user();
        $ownVehicles = $user->vehicles()->get();
        return $ownVehicles;
    }
}
