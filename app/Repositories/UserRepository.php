<?php

namespace App\Repositories;

use App\Models\User;
use App\Contracts\UserRepositoryInterface;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Auth;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function getVehicleOwners()
    {
        $vehicleOwners = User::role('vehicle owner')->orderBy('id')->get();

        return $vehicleOwners;
    }

    public function getWorkshopOwners()
    {
        $workshopOwners = User::role('workshop owner')->orderBy('id')->get();
        return $workshopOwners;
    }
}
