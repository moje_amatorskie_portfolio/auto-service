<?php

namespace App\Services;

use App\Services\StringGeneratorService;

class VinGeneratorService
{
    private $stringGeneratorService;

    public function __construct()
    {
        $this->stringGeneratorService = new StringGeneratorService();
    }
    public function vinGeneratorService()
    {
        $permittedAlphaUppercase = 'ABCDEFGHJKLMNOPQRSTUVWXYZ';
        $vin = rand(1, 9);
        $vin .= $this->stringGeneratorService->generate_string($permittedAlphaUppercase, 4);
        $vin .= rand(10, 99);
        $vin .= $this->stringGeneratorService->generate_string($permittedAlphaUppercase, 4);
        $vin .= rand(100000, 999999);
        return $vin;
    }
}
