<?php

namespace App\Services;

use App\Services\VinGeneratorService;
use App\Services\StringGeneratorService;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class VehicleGeneratorService
{
    private $vinGeneratorService;
    private $stringGeneratorService;

    public function __construct()
    {
        $this->vinGeneratorService = new VinGeneratorService();
        $this->stringGeneratorService = new StringGeneratorService();
    }

    public function vehicleGeneratorService()
    {
        $vehicle = array();
        $makes = ['BMW', 'Fiat', 'Daewoo', 'VW', 'Ford', 'Jaguar', 'Star', 'Mini', 'Volga', 'Matsui', 'Skoda', 'Mitsubishi', 'Dacha', 'Jelcz','Honda', 'Ferrari', 'Acura', 'Alfa Romeo', 'Audi', 'Bentley', 'Cadillac', 'Chrysler', 'Ford', 'Hyundai', 'Jeep', 'Kia', 'Land Rover', 'Lexus', 'Lotus', 'Maserati', 'Mazda', 'Pontiac', 'Porsche', 'Subaru', 'Suzuki', 'Toyota', 'Volvo'];
        $vehicle['make'] = Arr::shuffle($makes)[0];
        $vehicle['model'] = Str::upper(Str::random(6));
        $vehicle['fuel'] = rand(0, 1) ? 'petrol' : 'diesel';
        $vehicle['capacity'] = rand(100, 200);
        $vehicle['power'] = rand(200, 300);
        $vehicle['vin'] = $this->vinGeneratorService->vinGeneratorService();
        $vehicle['plate'] = $this->stringGeneratorService->generate_string('SERGSEDFXV', 3)." ".rand(11111, 99999);
        $vehicle['equipment'] = 'Equipment';
        $vehicle['description'] = 'Description';
        return $vehicle;
    }
}
