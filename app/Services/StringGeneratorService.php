<?php

namespace App\Services;

class StringGeneratorService
{
    public function generate_string($permitted_chars, $numberOfChars = 1)
    {
        $input_length = strlen($permitted_chars);
        $random_string = '';
        for ($i = 0; $i < $numberOfChars; $i++) {
            $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
        return $random_string;
    }
}
