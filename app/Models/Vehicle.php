<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Image\Manipulations;
use App\Models\Service;

class Vehicle extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $appends = ['vehicle_owner'];
    protected $guarded = [];



    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')

              ->fit(Manipulations::FIT_CROP, 250, 250)
              ;
    }

    public function getVehicleOwnerAttribute()
    {
        $vehicleOwnerModel = $this->users()->firstOrFail();
        $vehicleOwnerFirstName = $vehicleOwnerModel->first_name;
        $vehicleOwnerLastName = $vehicleOwnerModel->last_name;
        $vehicleOwnerName = strtoupper(substr($vehicleOwnerFirstName, 0, 1)).'.'.$vehicleOwnerLastName;

        return $vehicleOwnerName;
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function sharedWithUsers()
    {
        return $this->belongsToMany(User::class, 'shared_vehicles');
    }

    public function workshops()
    {
        return $this->belongsToMany(Workshop::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
