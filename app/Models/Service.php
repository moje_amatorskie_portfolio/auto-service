<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Vehicle;
use App\Models\Workshop;

class Service extends Model
{
    use HasFactory;

    public function vehicle()
    {
        $this->belongsTo(Vehicle::class);
    }

    public function workshop()
    {
        $this->belongsTo(Workshop::class);
    }
}
