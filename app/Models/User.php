<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasFactory;
    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'password_confirmation',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:Y-m-d H:i',
    ];

    protected $appends = ['full_name', 'workshop', 'vehicle_count'];

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
    public function getWorkshopAttribute()
    {
        $workshop = 'workshop';
        if (!empty($this->workshop()->first()->name)) {
            $workshop = $this->workshop()->firstOrFail()->name;
        }
        return $workshop;
    }

    protected function getVehicleCountAttribute()
    {
        return $this->vehicles()->count();
    }

    public function workshop()
    {
        return $this->belongsTo(Workshop::class);
    }

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class);
    }

    public function sharedVehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'shared_vehicles');
    }

    public function settings()
    {
        return $this->hasMany(Settings::class);
    }
}
