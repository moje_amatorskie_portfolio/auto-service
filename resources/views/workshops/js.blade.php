<script src="/js/sweetalert2_vehicles.js"></script>
<script src="/js/notyf_toasts.js"></script>
<script src="/js/workshops_datatables.js"></script>
<script>

$(document).ready(function() {
    @can('create workshop')
        $(".row > div.eight.wide.column:first-child").prepend("\
            <form action='{{ route('workshops.create') }}' method='GET'>\
                <button class='ui medium olive basic button'><i class='fa fa-plus fa-fw'></i>&nbsp;Add</button>\
            </form>\
        ");
    @endcan
    $("#workshops_index tbody").on('click', 'td.sorting_1.dtr-control', function(event){
        showCrudButtons();
    });
    $('#workshops_index').on( 'draw.dt', function () {
        showCrudButtons();
    });
});

function deleteRow(element) {
    let id = element.getAttribute('data-id').substring(8);
    axios.post('/workshops/' + id, {
        _method: 'DELETE'
    })
    .then(function (response) {
        $("#workshops_index").DataTable().ajax.reload();
        notyfToast('Workshop deleted successfully ', 'success')
    })
    .catch(function (error) {
        notyfToast(error.response.data.message, 'error');
    });
}

function showCrudButtons(){
    @can('show workshop')
        $("a[id^=workshop_show").show();
    @endcan
    @can('edit workshop')
        $("a[id^=workshop_edit").show();
    @endcan
    @can('destroy workshop')
        $("button[id^=workshop_destroy").show();
    @endcan
}

</script>
