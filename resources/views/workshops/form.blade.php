@extends('layouts.app')

@section('content_header')
    <h1>
        {{ $formHeader }}
    </h1>
@stop

@section('content')
    @parent
    @push('css')
        <link rel='stylesheet' href='{{ asset('css/vehicles/form.css') }}'/>
        <link href="https://api.mapbox.com/mapbox-gl-js/v2.9.2/mapbox-gl.css" rel="stylesheet">
        <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v5.0.0/mapbox-gl-geocoder.css" type="text/css">
    @endpush

<style>
.mapboxgl-ctrl-geocoder {
    min-width: 100%;
}
</style>

    <div class='col-12 col-sm-7 col-md-6 col-lg-6 col-xl-4 m-auto'>
    <form spellcheck='false' id="workshop_form" action="{{ $route??"" }}" method="POST" class="ui form" enctype="multipart/form-data">
      @csrf
      @if(@$formType == 'edit')
        @method('PUT')
      @endif

      @if($errors->any())
      <div class="ui form error">
        <div class="ui error message">
          <div class="header">Whoops! There were some problems with your form. </div>
          @foreach ($errors->all() as $ErrorMessage)
            <div>{{ $ErrorMessage }}</div>
          @endforeach
          <p></p>
        </div>
      </div>
      @endif

      <h4 class="ui dividing header"></h4>

        <div class="d-flex flex-column">

            <div class="field @error('name') error @enderror col-12 required">
            <label>Name</label>
            <input name="name" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('name', $workshop->name??'') }}" type="text" maxlength="30" placeholder="Name" class="">
            </div>

            <input hidden id="place" name="place" />

            <div id='geocoder' class="field col-12 required">
                <label>City/Town</label>
            </div>

            <div class="field @error('longitude') error @enderror col-12 ">
              <label>Longitude</label>
              <input id="longitude" name="longitude" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('longitude') ?? $workshop->longitude??'' }}" type="text" maxlength="50" placeholder="Longitude" class="">
            </div>
            <div class="field @error('latitude') error @enderror col-12 ">
              <label>Latitude</label>
              <input id="latitude" name="latitude" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('latitude') ?? $workshop->latitude??'' }}" type="text" maxlength="50" placeholder="Latitude" class="">
            </div>
            <div class="field @error('street') error @enderror col-12 ">
              <label>Street</label>
              <input name="street" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('street') ?? $workshop->street??'' }}" type="text" maxlength="50" placeholder="Street" class="">
            </div>

        </div>

      <h4 class="ui dividing header"></h4>

      @if($formType == 'show')
        <a href="{{ $route??'' }}" >
            <div class="ui button">Back</div>
        </a>
      @else
        <button class="ui button" type="submit" tabindex="0">Save</button>
      @endif
      </button>
    </form>
    </div>

@endsection

@push('js')
    @include('workshops.form_js')
@endpush
