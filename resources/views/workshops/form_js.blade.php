<script src="https://api.mapbox.com/mapbox-gl-js/v2.9.2/mapbox-gl.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v5.0.0/mapbox-gl-geocoder.min.js"></script>

<script>

mapboxgl.accessToken = "{{ env('MAPBOX_KEY') }}";

const geocoder = new MapboxGeocoder({
accessToken: mapboxgl.accessToken,
types: 'country,region,place,postcode,locality,neighborhood'
});

geocoder.addTo('#geocoder');

$(document).ready(function(){
    @if(!empty($disabled))
    jQuery(".mapboxgl-ctrl-geocoder--input").addClass("no-pointer-events");
    @endif
    jQuery(".mapboxgl-ctrl-geocoder--input").val("{{ old('place') ?? $workshop->place??'' }}");
    $(".mapboxgl-ctrl-geocoder--input").on("change", function(){
        var place = $(".mapboxgl-ctrl-geocoder--input").val();
    });
    geocoder.on('result', (e) => {
        console.log(JSON.stringify(e.result, null, 2));
        $("#place").val(e.result.place_name);
        $("#longitude").val(e.result.center[0]);
        $("#latitude").val(e.result.center[1]);
    });
});
</script>
