@extends('layouts.app')

@section('content_header')
    <h1>Workshops</h1>
@stop

@section('content')
@parent
    @include('workshops.datatables')
@endsection

@push('js')
    @include('workshops.js')
@endpush
