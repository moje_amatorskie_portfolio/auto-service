@extends('layouts.app')

@push('css')
    @include('map.css')
@endpush

@section('content_header')
@stop

@section('content')
    @include('map.map')
@parent

@endsection

@push('js')
    @include('map.js')
@endpush
