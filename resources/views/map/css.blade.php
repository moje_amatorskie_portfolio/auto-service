<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v2.9.2/mapbox-gl.css' rel='stylesheet' />

<style>
  body {
    margin: 0;
    padding: 0;
  }

  #map {
    position: absolute;
    top: 0px;
    bottom: 0;
    width: 100%;
  }

  .marker {
  background-image: url("{{ asset('images/marker.png') }}");
  background-size: cover;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  cursor: pointer;
}

.mapboxgl-popup {
  max-width: 200px;
  background-color: lightgray;

}

.mapboxgl-popup-content {
  text-align: center;
  font-family: 'Open Sans', sans-serif;
  background-color: lightgray;
}

</style>
