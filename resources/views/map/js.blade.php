<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v2.9.2/mapbox-gl.js'></script>

<script>

mapboxgl.accessToken = '{{ env('MAPBOX_KEY') }}';

const map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v11',
  center: [19, 52],
  zoom: 6
});

var geojson = {
  type: 'FeatureCollection',
  features : []
};

axios.get('/services/api/index')
  .then(function (response) {
    for(const service of response.data){
        var is_approved = 'pending';
        if(service.is_approved == '1'){
            is_approved = 'approved';
        }
        if(service.is_approved == '2'){
            is_approved = 'denied';
        }
        geojson.features.push({type: 'Feature', geometry: {type: 'Point', coordinates: [service.longitude, service.latitude]},properties: {title: service.title, description: service.description, start: 'start: ' + service.start.substring(0,16),  end: 'end: ' + service.end.substring(0,16), 'owner': service.owner, place: service.place + ', ' + service.street, make: service.make + ' ' + service.model, workshop: service.workshop, approval: is_approved}});
    }
    for (const feature of geojson.features) {
        new mapboxgl.Marker({ color: 'orange', rotation: 0 })
         .setLngLat(feature.geometry.coordinates)
         .setPopup(
            new mapboxgl.Popup({ offset: 25 })
              .setHTML(
            `<h3>${feature.properties.title}</h3><p>${feature.properties.description}</p><p>${feature.properties.start}</p><p>${feature.properties.end}</p><p>${feature.properties.owner}</p><p>${feature.properties.place}</p><p>${feature.properties.make}</p><p>${feature.properties.workshop}</p><p>${feature.properties.approval}</p>`
            )
        )
          .addTo(map);
    }
  })
  .catch(function (error) {
    //
    console.log(error);
  })
  .then(function () {
    //
  });


</script>
