<script>
    $('#vehicle_owner_button').on('click', function() {
        $('#email_input').val('vehicle.owner@vehicle.owner.com');
    });
    $('#workshop_owner_x_button').on('click', function() {
        $('#email_input').val('workshop.owner_x@workshop.owner.com');
    });
    $('#workshop_owner_y_button').on('click', function() {
        $('#email_input').val('workshop.owner_y@workshop.owner.com');
    });
    $('#superadmin_button').on('click', function() {
        $('#email_input').val('superadmin@superadmin.com');
    });
    $('#kowalski_button').on('click', function() {
        $('#email_input').val('jan.kowalski@jan.kowalski.com');
    });
    $('#nowak_button').on('click', function() {
        $('#email_input').val('marcin.nowak@marcin.nowak.com');
    });
    $('#kowalczyk_button').on('click', function() {
        $('#email_input').val('marek.kowalczyk@marek.kowalczyk.com');
    });
</script>
