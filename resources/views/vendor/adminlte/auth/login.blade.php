@extends('adminlte::auth.auth-page', ['auth_type' => 'login'])

@section('adminlte_css_pre')
    <link rel="stylesheet" href="{{ asset('vendor/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@stop

@php( $login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login') )
@php( $register_url = View::getSection('register_url') ?? config('adminlte.register_url', 'register') )
@php( $password_reset_url = View::getSection('password_reset_url') ?? config('adminlte.password_reset_url', 'password/reset') )

@if (config('adminlte.use_route_url', false))
    @php( $login_url = $login_url ? route($login_url) : '' )
    @php( $register_url = $register_url ? route($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? route($password_reset_url) : '' )
@else
    @php( $login_url = $login_url ? url($login_url) : '' )
    @php( $register_url = $register_url ? url($register_url) : '' )
    @php( $password_reset_url = $password_reset_url ? url($password_reset_url) : '' )
@endif

@section('auth_header', __('adminlte::adminlte.login_message'))

@section('auth_body')
    <form action="{{ $login_url }}" method="post">
        {{ csrf_field() }}

        {{-- Email field --}}
        <div class="input-group mb-3">
            <input id="email_input" type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                   {{--value="{{ old('email') }}"--}} value="superadmin@superadmin.com" placeholder="{{ __('adminlte::adminlte.email') }}" autofocus>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>
            @if($errors->has('email'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif
        </div>

        {{-- Password field --}}
        <div class="input-group mb-3">
            <input id="password_input" type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                   placeholder="{{ __('adminlte::adminlte.password') }}" value="password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>
            @if($errors->has('password'))
                <div class="invalid-feedback">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
            @endif
        </div>

        {{-- Login field --}}
        <div class="row">
            <div class="col-7">
                <div class="icheck-primary">
                    <input type="checkbox" name="remember" id="remember">
                    <label for="remember">{{ __('adminlte::adminlte.remember_me') }}</label>
                </div>
            </div>
            <div class="col-5">
                <!-- <button type=submit class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}"> -->
                <button type='submit' class="ui gray btn-block button">
                    <span class="fas fa-sign-in-alt"></span>
                    {{ __('adminlte::adminlte.sign_in') }}
                </button>
            </div>
        </div>

    </form>

@stop

@section('auth_footer')
    {{-- Password reset link --}}
    @if($password_reset_url)
        <p class="my-0">
            <a href="{{ $password_reset_url }}">
                {{ __('adminlte::adminlte.i_forgot_my_password') }}
            </a>
        </p>
    @endif

    {{-- Register link --}}
    @if($register_url)
        <p class="my-0">
            <a href="{{ $register_url }}">
                {{ __('adminlte::adminlte.register_a_new_membership') }}
            </a>
        </p>
    @endif
@stop

@section('login_buttons')
    <div class="w-100 mt-3">
        <div class="w-100">
            <button id="superadmin_button" class="ui gray button w-100">Superadmin</button>
        </div>
    </div>
    <div class="w-100 mt-3">
        <div class="w-100">
            <button id="vehicle_owner_button" class="ui gray button w-100">Vehicle Owner</button>
        </div>
    </div>
    <div class="w-100 mt-3">
        <div class="w-100">
            <button id="workshop_owner_x_button" class="ui gray button w-100">Workshop Owner X</button>
        </div>
    </div>
    <div class="w-100 mt-3">
        <div class="w-100">
            <button id="workshop_owner_y_button" class="ui gray button w-100">Workshop Owner Y</button>
        </div>
    </div>
    <div class="w-100 mt-3">
        <div class="w-100">
            <button id="nowak_button" class="ui gray button w-100">Marcin Nowak</button>
        </div>
    </div>
    <div class="w-100 mt-3">
        <div class="w-100">
            <button id="kowalczyk_button" class="ui gray button w-100">Marek Kowalczyk</button>
        </div>
    </div>
    <div class="w-100 mt-3">
        <div class="w-100">
            <button id="kowalski_button" class="ui gray button w-100">Jan Kowalski</button>
        </div>
    </div>
    <div class="w-100 mt-3">
        <div class="w-100">
            <a href="{{ url('/login-with-google') }}">
                <!-- <button id="google_login_button" class="ui gray button w-100">Login with google</button> -->
                <button class="ui google plus button w-100">
                  <!-- <i class="google plus icon"></i> -->
                    Login with Google
                </button>
            </a>
        </div>
    </div>
    <div class="w-100 mt-3">
        <div class="w-100">
            <a href="{{ url('/login-with-facebook') }}">
                <!-- <button id="facebook_login_button" class="ui gray button w-100">Login with facebook</button> -->
                <button class="ui facebook button w-100">
                  <!-- <i class="facebook icon"></i> -->
                    Login with Facebook
                </button>
            </a>
        </div>
    </div>
    <!-- <div class="w-100 mt-3">
        <div class="w-100">
            <a href="#">
                <button disabled class="ui black button w-100"> -->
                  <!-- <i class="facebook icon"></i> -->
                    <!-- Google and facebook logins used to work with a ssl domain which is currently expired -->
                <!-- </button>
            </a>
        </div>
    </div> -->

@stop

@push('js')
    @include('vendor.adminlte.auth.js')
@endpush
