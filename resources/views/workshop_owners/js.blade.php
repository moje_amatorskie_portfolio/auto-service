<script src="/js/sweetalert2_vehicles.js"></script>
<script src="/js/notyf_toasts.js"></script>
<script src="/js/workshop_owners_datatables.js"></script>
<script>

$(document).ready(function() {
    @can('create workshop owner')
        $(".row > div.eight.wide.column:first-child").prepend("\
            <form action='{{ route('workshop-owners.create') }}' method='GET'>\
                <button class='ui medium olive basic button'><i class='fa fa-plus fa-fw'></i>&nbsp;Add</button>\
            </form>\
        ");
    @endcan
    $("#workshop_owners_index tbody").on('click', 'td.sorting_1.dtr-control', function(event){
        showCrudButtons();
    });
    $('#workshop_owners_index').on( 'draw.dt', function () {
        showCrudButtons();
    });
});

function deleteRow(element) {
    let id = element.getAttribute('data-id').substring(8);
    axios.post('/workshop-owners/' + id, {
        _method: 'DELETE'
    })
    .then(function (response) {
        $("#workshop_owners_index").DataTable().ajax.reload();
        notyfToast('Workshop owner deleted successfully ', 'success')
    })
    .catch(function (error) {
        notyfToast(error.response.data.message, 'error');
    });
}

function showCrudButtons(){
    @can('show workshop owner')
        $("a[id^=workshop_owner_show").show();
    @endcan
    @can('edit workshop owner')
        $("a[id^=workshop_owner_edit").show();
    @endcan
    @can('destroy workshop owner')
        $("button[id^=workshop_owner_destroy").show();
    @endcan
}

</script>
