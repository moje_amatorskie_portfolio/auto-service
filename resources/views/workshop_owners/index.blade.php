@extends('layouts.app')

@section('content_header')
    <h1>Workshop Owners</h1>
@stop

@section('content')
@parent
    @include('workshop_owners.datatables')
@endsection

@push('js')
    @include('workshop_owners.js')
@endpush
