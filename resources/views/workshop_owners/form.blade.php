@extends('layouts.app')

@section('content_header')
    <h1>
        {{ $formHeader }}
    </h1>
@stop

@section('content')
    @parent
    @push('css')
        <link rel='stylesheet' href='{{ asset('css/vehicles/form.css') }}'/>
    @endpush
    <div class='col-12 col-sm-7 col-md-6 col-lg-6 col-xl-4 m-auto'>
    <form spellcheck='false' id="workshop_owner_form" action="{{ $route??"" }}" method="POST" class="ui form" enctype="multipart/form-data">
      @csrf
      @if(@$formType == 'edit')
        @method('PUT')
      @endif

      @if($errors->any())
      <div class="ui form error">
        <div class="ui error message">
          <div class="header">Whoops! There were some problems with your form. </div>
          @foreach ($errors->all() as $ErrorMessage)
            <div>{{ $ErrorMessage }}</div>
          @endforeach
          <p></p>
        </div>
      </div>
      @endif

      <h4 class="ui dividing header"></h4>

        <div class="d-flex flex-column">

            <div class="field @error('first_name') error @enderror col-12 required">
            <label>First name</label>
            <input name="first_name" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('first_name', $workshopOwner->first_name??'') }}" type="text" maxlength="30" placeholder="First name" class="">
            </div>

            <div class="field @error('last_name') error @enderror col-12 required">
              <label>Last name</label>
              <input name="last_name" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('last_name') ?? $workshopOwner->last_name??'' }}" type="text" maxlength="30" placeholder="Last name" class="">
            </div>

            <div class="field @error('email') error @enderror col-12 required">
              <label>Email</label>
              <input name="email" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('email') ?? $workshopOwner->email??'' }}" type="text" maxlength="30" placeholder="Email" class="">
            </div>

            <div class="field @error('address') error @enderror col-12 required">
              <label>Address</label>
              <input name="address" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('address') ?? $workshopOwner->address??'' }}" type="text" maxlength="30" placeholder="Address" class="">
            </div>
            @if(@$formType != 'show')
            <div class="field @error('password') error @enderror col-12 required">
              <label>Password</label>
              <input type='password' name="password" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('password')??'' }}" type="text" maxlength="30" placeholder="Password" class="">
            </div>

            <div class="field @error('password_confirmation') error @enderror col-12 required">
              <label>Password confirmation</label>
              <input type="password" name="password_confirmation" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('password_confirmation')??'' }}" type="text" maxlength="30" placeholder="Password confirmation" class="">
            </div>
            @endif

        </div>

      <h4 class="ui dividing header"></h4>

      @if($formType == 'show')
        <a href="{{ $route??'' }}" >
            <div class="ui button">Back</div>
        </a>
      @else
        <button class="ui button" type="submit" tabindex="0">Save</button>
      @endif
      </button>
    </form>
    </div>

@push('js')
@endpush

@endsection

@push('js')
@endpush
