@extends('layouts.app')

@section('content_header')
    <h1>
        {{ $formHeader }}
    </h1>
@stop

@section('content')
    @parent
    @push('css')
        <link rel='stylesheet' href='{{ asset('css/vehicles/form.css') }}'/>
    @endpush
        <div class='col-12 col-sm-7 col-md-6 col-lg-6 col-xl-4 m-auto'>
        <form spellcheck='false' id="change_password_form" action="{{ route('change.password') }}" method="POST" class="ui form">
        @csrf
        @method('PUT')

    @if($errors->any())
    <div class="ui form error">
    <div class="ui error message">
      <div class="header">Whoops! There were some problems with your form. </div>
      @foreach ($errors->all() as $ErrorMessage)
        <div>{{ $ErrorMessage }}</div>
      @endforeach
      <p></p>
    </div>
    </div>
    @endif

      <h4 class="ui dividing header"></h4>

        <div class="d-flex flex-column">

            <div class="field @error('current_password') error @enderror col-12 required">
                <label>Current password</label>
                <input name="current_password" type="password" maxlength="50" placeholder="Current password">
            </div>

            <div class="field @error('password') error @enderror col-12 required">
              <label>Password</label>
              <input type='password' name="password" type="password" maxlength="50" placeholder="Password" >
            </div>

            <div class="field @error('password_confirmation') error @enderror col-12 required">
              <label>Password confirmation</label>
              <input type="password" name="password_confirmation" type="password" maxlength="50" placeholder="Password confirmation">
            </div>

        </div>

      <h4 class="ui dividing header"></h4>

        <a href="{{ $route??'' }}" >
            <div class="ui button">Cancel</div>
        </a>
        <button class="ui button" type="submit" tabindex="0">Save</button>
    </form>
    </div>

@push('js')
@endpush

@endsection

@push('js')
@endpush
