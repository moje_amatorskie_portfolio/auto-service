@extends('layouts.guest')

@section('choose-user-type-buttons')
<div style='border:solid;border-color:red;' class='h-100 row align-items-center'>

    <div class="d-flex flex-column col-12 col-sm-10 col-md-11 col-lg-6 col-xl-5 m-auto">
        <div class="ui success message text-center">
          <div class="header">
            This is the first time you log in with this email.
          </div>
          <p>Please choose if you are a vehicle owner or workshop owner.</p>
        </div>
        <div style="position:relative;" class="ui placeholder segment ">
          <div class="ui two column stackable center aligned grid">
            <div class="ui vertical divider d-none d-md-block">Or</div>
            <div class="middle aligned row">
              <div class="column">
                <div class="ui icon header">
                  <i class="truck icon"></i>
                  Vehicle owner
                </div>

                <form action="{{ route('choose-user-type-save') }}" method="POST">
                    @csrf
                    <input hidden name='first_name' value="{{ $socialiteUser->user['first_name']??'' }}" />
                    <input hidden name='last_name' value="{{ $socialiteUser->user['last_name']??'' }}" />
                    <input hidden name='email' value="{{ $socialiteUser->user['email']??'' }}" />
                    <button class="btn btn-primary w-100" type='submit'>
                        Choose
                    </button>
                </form>

              </div>
              <div style="position:relative" class="d-block d-md-none ui divider">
                  <div style="position:absolute; left:50%; top:-7px; transform: translate(-50%);">
                      Or
                  </div>
              </div>
              <div class="column">
                <div class="ui icon header">
                  <i class="building icon"></i>
                  Workshop owner
                </div>

                <form action="{{ route('choose-user-type-save') }}" method="POST">
                    @csrf
                    <input hidden name='first_name' value="{{ $socialiteUser->user['first_name']??'' }}" />
                    <input hidden name='last_name' value="{{ $socialiteUser->user['last_name']??'' }}" />
                    <input hidden name='email' value="{{ $socialiteUser->user['email']??'' }}" />
                    <input hidden name='workshop_owner' value="1" />
                    <button class="btn btn-primary w-100" type='submit'>
                        Choose
                    </button>
                </form>

              </div>
            </div>
          </div>
        </div>

    </div>

</div>

@stop
