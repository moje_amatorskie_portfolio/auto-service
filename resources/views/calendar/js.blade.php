<script src='{{asset("js/fullcalendar/main.js")}}'></script>
<script src='{{asset("js/notyf_toasts.js")}}'></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        @if(Auth::user()->hasRole(['super admin', 'workshop owner']))
        editable: true,
        @endif

        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,listWeek'
        },
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        dateClick: function(info) {
            @if(Auth::user()->hasRole('vehicle owner'))
                showFormModal(info);
            @endif
        },
        eventClick: function(info) {
            info.el.style.borderColor = 'red';
            @if(Auth::user()->hasRole(['workshop owner', 'super admin']))
                showFormModal(info);
            @endif

        },
        height: 655,
        eventSources: [
          {
            url: '/calendar/api/index/0',
            method: 'GET',
            extraParams: {
              custom_param1: 'something',
              custom_param2: 'somethingelse'
            },
            failure: function() {
              console.log('there was an error while fetching events!');
            },
            color: 'yellow',
            backgroundColor: 'yellow',

        },
          {
            url: '/calendar/api/index/1',
            method: 'GET',
            failure: function() {
              console.log('there was an error while fetching events!');
            },
            color: 'green',
            backgroundColor: 'green',

        },
          {
            url: '/calendar/api/index/2',
            method: 'GET',
            failure: function() {
              console.log('there was an error while fetching events!');
            },
            color: 'red',
            textColor: 'white',
            backgroundColor: 'red',
            eventColor: 'red',
          }

      ],
    });
    calendar.render();
    $('.ui.dropdown').dropdown();

    $('#add_service_modal').on('click', function (e) {
        e.stopPropagation();
    });
    $("#add_service_form_save_button").on('click', function(){
        $("#add_service_form").submit();
        $('#add_service_modal').modal('hide');
    });
    $("#add_service_form_cancel_button, #modal_close_button").on('click', function(){
        $('#add_service_modal').modal('hide');
    });
    $("#add_service_form_approve_button").on('click', function(){
        location.reload();
        approveOrDeclineService(true);
        $('#add_service_modal').modal('hide');
    });
    $("#add_service_form_decline_button").on('click', function(){
        location.reload();
        approveOrDeclineService(2);
        $('#add_service_modal').modal('hide');
    });
});

function showFormModal(info){

    $("#add_service_modal").modal('show');
    setServiceDay(info.dateStr);
    @if(Auth::user()->hasRole(['super admin', 'workshop owner']))
    setServiceId(info.event.id);
    setVehicleMake(info.event.extendedProps.make);
    setVehicleOwner(info.event.extendedProps.owner);
    setServiceTitle(info.event.title);
    setServiceDescription(info.event.extendedProps.description);
    setServiceStart(info.event.startStr);
    setServiceEnd(info.event.endStr);
    @endif
}
function setServiceDay(date){
    $('#service_day').val(date);
}
function setServiceId(id){
    $('#service_id').val(id);
}
function setServiceTitle(title){
    $( "input[name='title']" ).val(title);
}
function setServiceDescription(description){
    $("input[name='description']").val(description);
}
function setServiceStart(start){
    $("input[name='start']").val(start.substring(11,16));
}
function setServiceEnd(end){
    $("input[name='end']").val(end.substring(11,16));
}
function setVehicleMake(vehicle){
    $("#vehicle_id").append(new Option(vehicle, vehicle.id));
}
function setVehicleOwner(owner){
    $("input[name='owner']").val(owner);
}
function approveOrDeclineService(isApproved){
    serviceId = $("#service_id").val();
    axios.post('/calendar/' + serviceId, {
        _method: 'PUT',
        serviceId: serviceId,
        isApproved: isApproved,
    })
    .then(function (response) {
      notyfToast('Your decision has been saved', 'success');
    })
    .catch(function (error) {
        notyfToast(error.response.data.message, 'error');
    });


}

</script>
