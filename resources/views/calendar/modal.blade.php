<!-- Request a new service modal -->
<div id="add_service_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Request a new service</h5>
        <button id="modal_close_button" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

@include('calendar.form')

      </div>
      <div class="modal-footer">
          <button id="add_service_form_cancel_button" type="button" class="ui button" data-dismiss="modal">Cancel</button>
          @if(Auth::user()->hasRole('vehicle owner'))
          <button id="add_service_form_save_button" type="button" class="ui secondary button" data-dismiss="modal">Save</button>
          @endif
          @if(Auth::user()->hasRole(['super admin', 'workshop owner']))
          <button id="add_service_form_decline_button" type="button" class="ui button" data-dismiss="modal"><span class="text-danger">Decline</span></button>
          <button id="add_service_form_approve_button" type="button" class="ui button" data-dismiss="modal"><span class="text-success">Approve</span></button>
          @endif
      </div>
    </div>
  </div>
</div>
