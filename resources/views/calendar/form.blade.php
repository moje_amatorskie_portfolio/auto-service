        <form id="add_service_form" class="ui form" method="POST" action="{{ route('calendar.store') }}">
        @csrf
          <div class="field">
            <label>Name</label>
            <input type="text" name="title" value='service name' placeholder="Name" class="{{ !empty($disabled) ? 'pointer-events-none' : '' }}">
          </div>
          <div class="field">
            <label>Description</label>
            <input value='description' type="text" name="description" placeholder="Description" class="{{ !empty($disabled) ? 'pointer-events-none' : '' }}">
          </div>
          @if(Auth::user()->hasRole(['super admin', 'workshop owner']))
          <div class="field">
            <label>Owner</label>
            <input value='owner' type="text" name="owner" placeholder="Owner" class="{{ !empty($disabled) ? 'pointer-events-none' : '' }}">
          </div>
          @endif
          <div class="field">
            <label>Start hour</label>
            <input value='11' type="text" name="start" placeholder="Start hour" class="{{ !empty($disabled) ? 'pointer-events-none' : '' }}">
          </div>
          <div class="field">
            <label>End hour</label>
            <input value='12' type="text" name="end" placeholder="End hour" class="{{ !empty($disabled) ? 'pointer-events-none' : '' }}">
          </div>
          <div class="field">
            <label>Vehicle</label>
            <select id ="vehicle_id" name="vehicle_id" class="ui fluid  dropdown  {{ !empty($disabled) ? 'pointer-events-none' : '' }}">
                @if(!empty($ownVehicles))
                    @foreach($ownVehicles as $ownVehicle)
                        <option value='{{ $ownVehicle->id }}'>
                            {{ $ownVehicle->make }}
                        </option>
                    @endforeach
                @endif
            </select>
          </div>
          <input hidden id="service_day" name="service_day" />
          <input hidden id="service_id" name="service_id" />
        </form>
