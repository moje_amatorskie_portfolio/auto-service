@extends('layouts.app')

@push('css')
    @include('calendar.css')
@endpush

@section('content_header')
    <h1>Calendar</h1>
@stop

@section('content')
@parent
    @include('calendar.calendar')
@endsection

@push('js')
    @include('calendar.js')
@endpush
