@extends('layouts.app')

@section('content_header')
    <h1>Users</h1>
@stop

@section('content')
@parent
    @include('users.datatables')
@endsection

@push('js')
    @include('users.js')
@endpush
