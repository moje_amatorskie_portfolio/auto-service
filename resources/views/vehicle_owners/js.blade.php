<script src="/js/sweetalert2_vehicles.js"></script>
<script src="/js/notyf_toasts.js"></script>
<script src="/js/vehicle_owners_datatables.js"></script>
<script>
$(document).ready(function() {
    @can('create vehicle owner')
        $(".row > div.eight.wide.column:first-child").prepend("\
            <form action='{{ route('vehicle-owners.create') }}' method='GET'>\
                <button class='ui medium olive basic button'><i class='fa fa-plus fa-fw'></i>&nbsp;Add</button>\
            </form>\
        ");
    @endcan
    $("#vehicle_owners_index tbody").on('click', 'td.sorting_1.dtr-control', function(event){
        showCrudButtons();
    });
    $('#vehicle_owners_index').on( 'draw.dt', function () {
        showCrudButtons();
    });
});


function deleteRow(element) {
    let id = element.getAttribute('data-id').substring(8);
    axios.post('/vehicle-owners/' + id, {
        _method: 'DELETE'
    })
    .then(function (response) {
        $("#vehicle_owners_index").DataTable().ajax.reload();
        notyfToast('Vehicle owner deleted successfully ', 'success')
    })
    .catch(function (error) {
        notyfToast(error.response.data.message, 'error');
    });
}

function showCrudButtons(){
    @can('show vehicle owner')
        $("a[id^=vehicle_owner_show").show();
    @endcan
    @can('edit vehicle owner')
        $("a[id^=vehicle_owner_edit").show();
    @endcan
    @can('destroy vehicle owner')
        $("button[id^=vehicle_owner_destroy").show();
    @endcan
}

</script>
