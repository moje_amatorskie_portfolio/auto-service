@extends('layouts.app')

@section('content_header')
    <h1>Vehicle Owners</h1>
@stop

@section('content')
@parent
    @include('vehicle_owners.datatables')
@endsection

@push('js')
    @include('vehicle_owners.js')
@endpush
