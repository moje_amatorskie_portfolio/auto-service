@extends('layouts.app')

@section('content_header')
    <h1>Services</h1>
@stop

@section('content')
@parent
    @include('services.datatables')
@endsection

@push('js')
    @include('services.js')
@endpush
