@extends('layouts.app')

@section('content_header')
    <h1>Vehicles</h1>
@stop

@section('content')
@parent
    <x-datatables.mydatatables>
    </x-datatables.mydatatables>
@endsection

@push('js')
    @include('vehicles.js')
@endpush
