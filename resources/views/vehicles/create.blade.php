@extends('layouts.app')

@section('content_header')
    <h1>
        {{ $formHeader }}
        <span class="  containter-fluid">
        @if($formType == 'create')
              <div class="d-none d-sm-block inline field float-right">
                    <div id="fillVehicleFormLabel" class="ui right pointing green basic label large">
                      Press this button to fill out the form
                    </div>
                    <button id="fillVehicleForm" class="ui positive button">Fill form</button>
              </div>
        @endif</span>
    </h1>
@stop

@section('content')
    @parent
    @include('vehicles.form')
@endsection

@push('js')
<script>

 window.myDropzone = new Dropzone("#vehicle-thumbnails", {
    url: "/vehicles/media",
    dictRemoveFile: "Delete",
    dictCancelUploadConfirmation: true,
    dictCancelUpload: "Cancel upload",
    @if($formType == "show")
        clickable: false,
    @else
        addRemoveLinks: true,
    @endif
    init: function () {
        @if(isset($vehicle) && $vehicle->document->count() > 0)
          var files = {!! json_encode($vehicle->document) !!};
          var currentIndex = 0;
          for (var i in files) {
            var file = files[i]
            this.displayExistingFile({name: file.file_name}, "/images/vehicles/" + file.custom_properties.id + "/conversions/" + file.name + "-thumb.jpg");
            if(!currentIndex) {
                $('#image__preview').attr('src', '/images/vehicles/' + file.file_name);
            }
            $('#add_vehicle_form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">');
            currentIndex++;
          }
        @else
            $('#image__preview').attr('src', '/images/default_vehicle.jpeg');
        @endif
    }
 });


var uploadedDocumentMap = {}
myDropzone.options = {...myDropzone.options,

  dictRemoveFile: "Delete",
  maxFilesize: 222,
  addRemoveLinks: true,
  headers: {
    'X-CSRF-TOKEN': "{{ csrf_token() }}"
  },
}

 myDropzone.on('success', function (file, response) {
    $('div.dz-details').hide();
    $('.dz-image > img').addClass('hover-cursor-pointer');
    $('#add_vehicle_form').append('<input type="hidden" name="document[]" value="' + response.name + '">');
    uploadedDocumentMap[file.name] = response.name;
    $(".dz-preview:last-child [data-dz-name").text(response.name);
    $('#image__preview').attr('src', '/images/vehicles/'+response.name);
    $('.dz-preview').off();
    $('.dz-preview').on('click', function(){
        let imagePreviewName = $(this).find('[data-dz-name]').html();
        $('#image__preview').attr('src', '/images/vehicles/'+imagePreviewName);
    });
});

myDropzone.on('removedfile', function (file) {
    file.previewElement.remove();
    var name = '';
    if (file.upload == undefined) {
      name = file.name
    } else {
      name = uploadedDocumentMap[file.name];
    }
    $('#add_vehicle_form').find('input[name="document[]"][value="' + name + '"]').remove()
    if(!$('input[name="document[]"]').length){
        $('#image__preview').attr('src', '/images/default_vehicle.jpeg');
    }
});

$(document).ready(function(){
    $('#fillVehicleForm').on('click', function() {
        fillForm();
        $('#fillVehicleFormLabel').hide();

    });
    $('div.dz-details').hide();
    $('.dz-image > img').addClass('hover-cursor-pointer');
    var splideCreated = false;
    $('.dz-preview').on('click', function(){
        let imagePreviewName = $(this).find('[data-dz-name]').html();
        $('#image__preview').attr('src', '/images/vehicles/'+imagePreviewName);
    });


    $('#image__preview').on('click', function(){
        let imageElements = $("input[name='document[]'");
        if(imageElements.length > 0) {
            createSplideBase();
            imageElements.each(function( index ) {
                $('.splide__list').append('<li class="splide__slide"><img style=\"margin:auto;height:100vh;\" src=\"/images/vehicles/'+$(this).val()+'\"/></li>');
            });
            window.SplideVehicleCarousel = new Splide('.splide',{
                type: 'loop',
                classes: {
            		prev  : 'bigger-arrows splide__arrow--prev  ',
            		next  : 'splide__arrow--next bigger-arrows',
                },
            }
            ).mount();
        }
    });
    $( "body" ).keyup(function( event ) {
        if ( event.which == 27 ) {
            event.preventDefault();
            $('.splide.splide--ltr.splide--draggable.is-active').remove();
        }
    });

    $('select.dropdown')
        .dropdown()
    ;



});

function fillForm() {
    axios.post('/generate-vehicle', {}
    )
    .then(function (response) {
        $('input[name="make"]').val(response.data.make);
        $('input[name="model"]').val(response.data.model);
        $('input[name="fuel"]').val(response.data.fuel);
        $('input[name="capacity"]').val(response.data.capacity);
        $('input[name="power"]').val(response.data.power);
        $('input[name="vin"]').val(response.data.vin);
        $('input[name="plate"]').val(response.data.plate);
        $('textarea[name="equipment"]').val(response.data.equipment);
        $('textarea[name="description"]').val(response.data.description);
    })
    .catch(function (error) {

    });
}

function createSplideBase() {
    var splideCarousel = '\
    <div style=\"position:absolute;top:0;left:0;width:100vw;height:100vh;z-index:1500;background:black\" class="splide">\
    	<div class="splide__track">\
    		<ul id=\'#splide_items_list\' class="splide__list">\
            \
    		</ul>\
    	</div>\
    </div>\
    ';
    $('body').append(splideCarousel);
}

</script>
@endpush
