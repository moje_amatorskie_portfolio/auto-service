<script src="/js/sweetalert2_vehicles.js"></script>
<script src="/js/notyf_toasts.js"></script>
<script src="/js/vehicle_datatables.js"></script>
<script>

$(document).ready(function() {
    @can('create vehicle')
        $(".row > div.eight.wide.column:first-child").prepend("\
            <form action='{{ route('vehicles.create') }}' method='GET'>\
                <button class='ui medium olive basic button'><i class='fa fa-plus fa-fw'></i>&nbsp;Add</button>\
            </form>\
        ");
    @endcan
    @if(Auth::user()->hasRole(['super admin', 'workshop owner', 'vehicle owner']))
        $(".row > div.eight.wide.column:first-child > form").append("\
                    <button id='pdf_list' class='ui medium brown basic button'>\
                        <i class='fa fa-download fa-fw'></i>\
                        &nbsp;List in PDF\
                    </button>\
        ");
        $( "#pdf_list" ).click(function( event ) {
            event.preventDefault();
            window.open('/vehicle_list/pdf', '_blank');
        });
    @endif

    $("#vehicles_index tbody").on('click', 'td.sorting_1.dtr-control', function(event){
        showCrudButtons();
        showOwnVehiclesCrudButtons();
    });
    $('#vehicles_index').on( 'draw.dt', function () {
        showCrudButtons();
        showOwnVehiclesCrudButtons();
    });
    @if(session('message'))
        notyfToast('{{ session('message') }}', 'success');
    @endif
});

function deleteRow(element) {
    let id = element.getAttribute('data-id').substring(8);
    axios.post('/vehicles/' + id, {
        _method: 'DELETE'
    })
    .then(function (response) {
        $("#vehicles_index").DataTable().ajax.reload();
        notyfToast('Vehicle deleted successfully ', 'success')
    })
    .catch(function (error) {
        notyfToast(error.response.data.message, 'error');
    });
}

function showCrudButtons(){
    @can('show vehicle')
        $("a[id^=vehicle_show").show();
    @endcan

}

function showOwnVehiclesCrudButtons(){
    var ownVehiclesIds = [];
    @if(!empty($ownVehicles))
        @foreach($ownVehicles as $ownVehicle)
            ownVehiclesIds.push({{ $ownVehicle->id }});
        @endforeach
    @endif
    @if(Auth::user()->hasRole('super admin'))
        $("a[id^=vehicle_edit").show();
        $("button[id^=vehicle_destroy").show();
    @elseif(Auth::user()->hasRole('workshop owner'))
        $("button[id^=vehicle_destroy").show();
    @else
    for(const ownVehicleId of ownVehiclesIds){
        $("a[data-id='edit-" + ownVehicleId + "']").show();
        $("button[data-id='destroy-" + ownVehicleId + "']").show();
    }
    @endif
}

</script>
