@push('css')
    <link rel='stylesheet' href='{{ asset('css/dropzone.css') }}'/>
    <link rel='stylesheet' href='{{ asset('css/splide.css') }}'/>
    <link rel='stylesheet' href='{{ asset('css/vehicles/form.css') }}'/>
@endpush

<form spellcheck='false' id="add_vehicle_form" action="{{ $route??"" }}" method="POST" class="ui form container" enctype="multipart/form-data">
  @csrf
  @if(@$formType == 'edit')
    @method('PUT')
  @endif

  @if($errors->any())
  <div class="ui form error">
    <div class="ui error message">
      <div class="header">Whoops! There were some problems with your form. </div>
      @foreach ($errors->all() as $ErrorMessage)
        <div>{{ $ErrorMessage }}</div>
      @endforeach
      <p></p>
    </div>
  </div>
  @endif
  <h4 class="ui dividing header"></h4>
    <div class="row">
      <div class="field @error('make') error @enderror col-12 col-sm-6 col-lg-3 required">
        <label>Make</label>
        <input name="make" style="{{ !empty($disabled) ? 'pointer-events:none;' : '' }}" value="{{ old('make') ?? $vehicle->make??'' }}" type="text" maxlength="30" placeholder="Make" class="">
      @error('make')

        @foreach($errors->get('make') as $errorMessage)
          <!-- {{ $errorMessage }} -->
        @endforeach
      @enderror
      </div>

      <div class="field @error('model') error @enderror col-12 col-sm-6 col-lg-3 required">
        <label>Model</label>
        <input name="model"  value="{{ old('model') ?? $vehicle->model??'' }}" type="text" maxlength="30" placeholder="Model" class="{{ !empty($disabled) ? 'no-pointer-events' :'' }}">
      </div>
      <div class="field @error('fuel') error @enderror col-12 col-sm-6 col-lg-3 required">
        <label>Fuel</label>
        <div class="">
          <div class="">
            <select name="fuel" class="ui fluid  dropdown  {{ !empty($disabled) ? 'no-pointer-events' : '' }}">
              @if(!empty(old('fuel')))
                <option {{ collect(old('fuel'))->contains('petrol') ? 'selected' : ''}} value='petrol'>petrol</option>
                <option {{ collect(old('fuel'))->contains('diesel') ? 'selected' : ''}} value='diesel'>diesel</option>
              @elseif(@$formType != 'create')
                <option {{ (!empty($vehicle->fuel) && $vehicle->fuel=="petrol") ? "selected" : "" }} value="petrol">petrol</option>
                <option {{ (!empty($vehicle->fuel) && $vehicle->fuel=="diesel") ? "selected" : "" }} value="diesel">diesel</option>
              @else
                <option value='petrol'>petrol</option>
                <option value='diesel'>diesel</option>
              @endif
            </select>
          </div>
      </div>
      </div>
      <div class="field @error('capacity') error @enderror col-12 col-sm-6 col-lg-3">
         <label>Capacity</label>
         <input name="capacity" value="{{ old('capacity') ?? $vehicle->capacity??'' }}" type="text" maxlength="10" placeholder="Capacity" class="{{ !empty($disabled) ? 'no-pointer-events' : '' }}">
      </div>
    </div>

    <div class="row">
      <div class="field @error('power') error @enderror col-12 col-sm-6 col-lg-3 ">
        <label>Power</label>
        <input name="power" value="{{ old('power') ?? $vehicle->power??'' }}" type="text" maxlength="10" placeholder="Power" class="{{ !empty($disabled) ? 'no-pointer-events' : '' }}">
      </div>
      <div class="field @error('vin') error @enderror col-12 col-sm-6 col-lg-3  required">
        <label>VIN</label>
        <input name="vin" value="{{ old('vin') ?? $vehicle->vin??'' }}" type="text" maxlength="20" placeholder="VIN" class="{{ !empty($disabled) ? 'no-pointer-events' : '' }}">
      </div>
      <div class="field @error('plate') error @enderror col-12 col-sm-6 col-lg-3 required">
        <label>Plate</label>
        <input style="" name="plate" value="{{ old('plate') ?? $vehicle->plate??'' }}" type="text" maxlength="20" placeholder="Plate" class="{{ !empty($disabled) ? 'no-pointer-events' : '' }}">
      </div>
      <div class="field @error('shared_with') error @enderror col-12 col-sm-6 col-lg-3">
        <label>
            <strong>
                @if( @$formType != 'create' && !empty($vehicle->users()) )
                    {{ $vehicle->users()->firstOrFail()->full_name }}
                @else
                    The owner
                @endif
            </strong>
            shares this car with
        </label>
        <div class="">
          <div class="">
            <select id="shared_with_users" name="shared_with_users[]" multiple="multiple" class="ui fluid  dropdown  {{ !empty($disabled) ? 'no-pointer-events' : '' }}">
              <option value="">Select</option>
              @foreach($vehicleOwners as $key => $vehicleOwner)
                @if(!empty(old('shared_with_users')))
                  <option value="{{ $vehicleOwner->id }}" {{  collect(old('shared_with_users'))->contains($vehicleOwner->id)  ? 'selected' : '' }}>
                @elseif(@$formType != 'create')
                  <option value="{{ $vehicleOwner->id }}" {{  $vehicle->sharedWithUsers()->get()->contains($vehicleOwner) ? 'selected' : '' }}>
                @else
                  <option value="{{ $vehicleOwner->id }}">
                @endif
                {{ $vehicleOwner->full_name }}
                </option>
              @endforeach

            </select>

            <!-- <select id="shared_with_users" name="shared_with_users[]" multiple></select> -->

            <!-- <div class="ui multiple selection dropdown">
                <input name="shared_with_users" hidden>
                <i class="dropdown icon"></i>
                <div class ="menu">
                  @foreach($vehicleOwners as $key => $vehicleOwner)

                    <div class="item" data-value="{{ $vehicleOwner->id }}" {{  ( collect(old('shared_with_users'))->contains($vehicleOwner->id) ) ? 'selected' : '' }}>
                        {{ $vehicleOwner->full_name }}
                    </div>
                  @endforeach
              </div>
            </div> -->

          </div>
      </div>
      </div>
    </div>

    <div class="row">

      <div class=" field @error('equipment') error @enderror col-12 col-sm-4 col-lg-4">
        <label>Equipment</label>
        <textarea name="equipment" style="resize:none" rows="4" placeholder="Equipment" class="{{ !empty($disabled) ? 'no-pointer-events' : '' }}">{{ old('equipment') ?? $vehicle->equipment??'' }}</textarea>
      </div>

      <div class="field @error('description') error @enderror col-12 col-sm-4 col-lg-4">
        <label>Description</label>
        <textarea name="description" style="resize:none" rows="4" placeholder="Description" class="{{ !empty($disabled) ? 'no-pointer-events' : '' }}">{{ old('description') ?? $vehicle->description??'' }}</textarea>
      </div>

        <div class="col-12 col-sm-4 col-lg-4">
          <label>Workshop</label>
          <select id="workshop" name="workshop" class="ui fluid  dropdown  {{ !empty($disabled) ? 'no-pointer-events' : '' }}">
            <option value="">Select</option>
            @foreach($workshops as $workshop)
              @if(!empty(old('workshop')))
                <option value="{{ $workshop->id }}" {{  old('workshop') == $workshop->id ? 'selected' : '' }}>
              @elseif(@$formType != 'create')
                <option value="{{ $workshop->id }}" {{ $vehicle->workshops()->firstOrFail()->id == $workshop->id ? 'selected' : '' }}>
              @else
                <option value="{{ $workshop->id }}" {{ $loop->first ? 'selected' : '' }}>
              @endif
              {{ $workshop->name }}
              </option>
            @endforeach
          </select>
        </div>



    </div>
    <div class="row">
      <div  class=" col-12 col-lg-6 ">
        <label>Thumbnails</label>

        <!-- class="dropzone" must be here even when dropzone is created imperatively in js -->
         <div style="border-radius:.285rem;height:398px;overflow:auto;overflow-x:hidden;" class="dropzone" id="vehicle-thumbnails">

<!-- <img src="{{ asset('images/vendor/datatables.net-dt/sort_both.png') }}" alt="Click me to remove the file." data-dz-remove /> -->



         </div> <!-- I am using div instead of form because dropzone is not showing -->
      </div>
      <div class=" col-12 col-lg-6">
        <label>Image</label>
        <div style="border-radius:.285rem;height:398px;background-color:white;border:2px solid rgba(0, 0, 0, 0.3);">
            <img class="hover-cursor-pointer" style="width:100%;height:100%" id="image__preview" src="" />
        </div>
      </div>
    </div>



  <h4 class="ui dividing header"></h4>


  @if($formType == 'show')
    <a href="{{ $route??'' }}" ><div class="ui button">Back</div></a>
  @else
    <button class="ui button" type="submit" tabindex="0">Save</button>
  @endif
  </button>
</form>

@push('js')
    <script src='{{ asset('js/dropzone.js') }}'></script>
    <script src='{{ asset('js/splide.js') }}'></script>

@endpush
