<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Vehicle list</title>
    </head>
    <body>
        <table style="width:100%;border-radius:15px;background:white;" id="vehicles_index" class="dt-responsive order-column hover nowrap">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Fuel</th>
                    <th>Capacity</th>
                    <th>Power</th>
                    <th>VIN</th>
                    <th>Plate</th>
                    <th>Owner</th>
                </tr>
            </thead>
            <tbody>
                @foreach($vehicles as $vehicle)
                <tr>
                    <td>{{ $vehicle->id }} </td>
                    <td>{{ $vehicle->make }} </td>
                    <td>{{ $vehicle->model }} </td>
                    <td>{{ $vehicle->fuel }} </td>
                    <td>{{ $vehicle->capacity }} </td>
                    <td>{{ $vehicle->power }} </td>
                    <td>{{ $vehicle->vin }} </td>
                    <td>{{ $vehicle->plate }} </td>
                    <td>{{ $vehicle->vehicle_owner }} </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
