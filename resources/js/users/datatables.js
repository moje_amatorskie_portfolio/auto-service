$.extend($.fn.dataTable.defaults, {
    searching: true,
    ordering: true,
    processing: true,
});

$('#users_index').DataTable({
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal({}),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                tableClass: 'ui table'
            })
        }
    },
    pageLength: 11,
    ordering: true,
    paging: true,
    lengthChange: false,
    deferRender: true,
    order: [0, 'desc'],
    ajax: {
        url: 'users/api/index',
        type: 'GET',
        rowId: 'id',
        dataSrc: '',
    },
    columns: [{
            data: 'id',
            responsivePriority: 1,
            name: 'id',
            type: 'num',
        },
        {
            data: 'full_name',
            responsivePriority: 2,

        },
        {
            data: 'email',
            responsivePriority: 3,
            defaultContent: "Workshop name",
        },
        {
            data: 'role',

        },
        {
            data: 'address',
            orderable: true
        },
    ],
    renderer: {
        header: "bootstrap",
        pageButton: "jqueryui"
    },
    pagingType: 'full_numbers',
});