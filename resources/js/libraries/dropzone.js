import Dropzone from "dropzone";

Dropzone.autoDiscover = false;

window.Dropzone = Dropzone;