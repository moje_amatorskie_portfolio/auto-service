$.extend($.fn.dataTable.defaults, {
    searching: true,
    ordering: true,
    processing: true,
});

$('#workshop_owners_index').DataTable({
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal({}),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                tableClass: 'ui table'
            })
        }
    },
    pageLength: 11,
    ordering: true,
    paging: true,
    lengthChange: false,
    deferRender: true,
    order: [0, 'desc'],
    ajax: {
        url: 'workshop-owners/api/index',
        type: 'GET',
        rowId: 'id',
        dataSrc: '',
    },
    columns: [{
            data: 'id',
            responsivePriority: 1,
            name: 'id',
            type: 'num',
        },
        {
            data: 'full_name',
            responsivePriority: 2,

        },
        {
            data: 'workshop',
            responsivePriority: 3,
            defaultContent: "Workshop name",
        },
        {
            data: 'email',

        },
        {
            data: 'created_at',
            orderable: true
        },
        {
            responsivePriority: 4,
            width: '20%',
            className: "dt-head-center dt-body-right",
            render: function(data, type, row, meta) {
                return "\
                <a style='display:none' id='workshop_owner_show_" + row.id + "' href='/workshop-owners/" + row.id + "'><button  class='ui mini olive basic button'><i class='fa fa-eye fa-fw'></i>&nbsp;View</button></a>\
                <a style='display:none' id='workshop_owner_edit_" + row.id + "' href='/workshop-owners/" + row.id + "/edit'><button  class='ui mini teal basic button'><i class='fa fa-edit fa-fw'></i>&nbsp;Edit</button></a>\
                <button style='display:none' id='workshop_owner_destroy_" + row.id + "' data-id='destroy-" + row.id + "' onclick='rowDeleteConfirm(this);' class='vehicle-delete ui mini orange basic button'><i class='fa fa-trash fa-fw'></i>&nbsp;Delete</button>\
                ";
            },
            defaultContent: "",
        },
    ],
    renderer: {
        header: "bootstrap",
        pageButton: "jqueryui"
    },
    pagingType: 'full_numbers',
});