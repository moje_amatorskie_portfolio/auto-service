$.extend($.fn.dataTable.defaults, {
    searching: true,
    ordering: true,
    processing: true,
});

$('#workshops_index').DataTable({
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal({}),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                tableClass: 'ui table'
            })
        }
    },
    pageLength: 11,
    ordering: true,
    paging: true,
    lengthChange: false,
    deferRender: true,
    order: [0, 'desc'],
    ajax: {
        url: 'workshops/api/index',
        type: 'GET',
        rowId: 'id',
        dataSrc: '',
    },
    columns: [{
            data: 'id',
            responsivePriority: 1,
            name: 'id',
            type: 'num',
        },
        {
            data: 'name',
            responsivePriority: 2,

        },
        {
            data: 'address',
            responsivePriority: 3,
            defaultContent: "Address",
        },
        {
            data: 'vehicle owners',
            responsivePriority: 3,
            defaultContent: 6,
        },
        {
            data: 'vehicles',
            responsivePriority: 3,
            defaultContent: 8,
        },
        {
            data: 'suppliers',
            responsivePriority: 3,
            defaultContent: 7,
        },
        {
            data: 'created_at',
            orderable: true
        },
        {
            responsivePriority: 4,
            width: '20%',
            className: "dt-head-center dt-body-right",
            render: function(data, type, row, meta) {
                return "\
                <a style='display:none' id='workshop_show_" + row.id + "' href='/workshops/" + row.id + "'><button  class='ui mini olive basic button'><i class='fa fa-eye fa-fw'></i>&nbsp;View</button></a>\
                <a style='display:none' id='workshop_edit_" + row.id + "' href='/workshops/" + row.id + "/edit'><button  class='ui mini teal basic button'><i class='fa fa-edit fa-fw'></i>&nbsp;Edit</button></a>\
                <button style='display:none' id='workshop_destroy_" + row.id + "' data-id='destroy-" + row.id + "' onclick='rowDeleteConfirm(this);' class='vehicle-delete ui mini orange basic button'><i class='fa fa-trash fa-fw'></i>&nbsp;Delete</button>\
                ";
            },
            defaultContent: "",
        },
    ],
    renderer: {
        header: "bootstrap",
        pageButton: "jqueryui"
    },
    pagingType: 'full_numbers',
});