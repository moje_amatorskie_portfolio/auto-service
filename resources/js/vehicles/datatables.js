$.extend($.fn.dataTable.defaults, {
    searching: true,
    ordering: true,
    processing: true,
});

$('#vehicles_index').DataTable({
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal({

            }),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                tableClass: 'ui table'
            })
        }
    },
    pageLength: 11,
    ordering: true,
    paging: true,
    lengthChange: false,
    deferRender: true,
    ajax: {
        url: 'vehicles/api/index',
        type: 'GET',

        rowId: 'id',
        dataSrc: '',
    },
    columns: [{
            data: 'id',
            responsivePriority: 1,
            name: 'id',
            type: 'num',
        },
        {
            data: 'make',
            responsivePriority: 2,
        },
        {
            data: 'model',
            responsivePriority: 3,
        },
        {
            data: 'fuel',
            "defaultContent": "this will be displayed if value from ajax json is null(data)",
        },
        {
            data: 'capacity',
            orderable: true
        },
        {
            data: 'power',
            orderable: true
        },
        {
            data: 'vin',
            responsivePriority: 7,
        },
        {
            data: 'plate',
            responsivePriority: 6,
            orderable: true
        },
        {
            data: 'vehicle_owner',
            responsivePriority: 5,
            orderable: true
        },
        {
            responsivePriority: 4,
            width: '20%',
            className: "dt-head-center dt-body-right",
            render: function(data, type, row, meta) {
                return "\
                <a style='display:none' id='vehicle_show_" + row.id + "' href='/vehicles/" + row.id + "'><button  class='ui mini olive basic button'><i class='fa fa-eye fa-fw'></i>&nbsp;View</button></a>\
                <a style='display:none' id='vehicle_edit_" + row.id + "' href='/vehicles/" + row.id + "/edit' data-id='edit-" + row.id + "'>\
                  <button class='ui mini teal basic button'><i class='fa fa-edit fa-fw'></i>&nbsp;Edit</button></a>\
                <button style='display:none' id='vehicle_destroy_" + row.id + "' data-id='destroy-" + row.id + "' onclick='rowDeleteConfirm(this);' class='vehicle-delete ui mini orange basic button'><i class='fa fa-trash fa-fw'></i>&nbsp;Delete</button>\
                ";
            },
            defaultContent: "",
        },
    ],
    renderer: {
        header: "bootstrap",
        pageButton: "jqueryui"
    },
    pagingType: 'full_numbers',

});