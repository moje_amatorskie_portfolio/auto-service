$.extend($.fn.dataTable.defaults, {
    searching: true,
    ordering: true,
    processing: true,
});

$('#services_index').DataTable({
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal({}),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
                tableClass: 'ui table'
            })
        }
    },
    pageLength: 11,
    ordering: true,
    paging: true,
    lengthChange: false,
    deferRender: true,
    order: [0, 'desc'],
    ajax: {
        url: 'services/api/index',
        type: 'GET',
        rowId: 'id',
        dataSrc: '',
    },
    columns: [{
            data: 'id',
            responsivePriority: 1,
            name: 'id',
            type: 'num',
        },
        {
            data: 'title',
            responsivePriority: 2,

        },
        {
            data: 'owner',
            responsivePriority: 2,

        },
        {
            data: 'description',
            responsivePriority: 3,
            defaultContent: "8",
        },
        {
            data: 'start',

        },
        {
            data: 'end',
            orderable: true
        },
        {
            data: 'make',
            orderable: true
        },
        {
            data: 'is_approved',
            render: function(data, type, row, meta) {
                if (data == '0') {
                    return "<span class='text-warning'><bold>PENDING</bold></span>";
                }
                if (data == '1') {
                    return "<span class='text-success'><bold>APPROVED</bold></span>";
                }
                if (data == '2') {
                    return "<span class='text-danger'><bold>DECLINED</bold></span>";
                }

            },
            orderable: true
        },
    ],
    renderer: {
        header: "bootstrap",
        pageButton: "jqueryui"
    },
    pagingType: 'full_numbers',
});