import {
    Notyf
} from 'notyf';
import 'notyf/notyf.min.css';

const notyf = new Notyf({
    duration: 1500,
    ripple: true,
    position: {
        x: 'center',
        y: 'top',
    },
    types: [{
            type: 'warning',
            background: 'orange',
            icon: {
                className: 'material-icons',
                tagName: 'i',
                text: 'warning'
            }
        },
        {
            type: 'error',
            background: 'indianred',
            duration: 6000,
            dismissible: true
        },
    ]
});

window.notyfToast = (message, type) => {
    if (type == 'success') {
        notyf.success(message);
    }
    if (type == 'error') {
        notyf.error(message);
    }
}