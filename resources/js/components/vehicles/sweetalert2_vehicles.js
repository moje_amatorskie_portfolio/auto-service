import Swal from 'sweetalert2';

window.rowDeleteConfirm = function(element)
{
    Swal.fire({
        icon: 'warning',
        text: 'Are you sure that you want to delete no. ' + element.getAttribute('data-id') + '?',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        confirmButtonColor: '#e3342f',
    }).then((result) => {
        if (result.isConfirmed) {
            deleteRow(element);
        }
    });
}
