<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



use App\Http\Controllers\HomeController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\WorkshopController;
use App\Http\Controllers\VehicleOwnerController;
use App\Http\Controllers\WorkshopOwnerController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\Auth\ChangePasswordController;
use App\Http\Controllers\Auth\SocialiteController;
use App\Http\Controllers\Auth\GoogleLoginController;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::apiResource('/vehicles', VehicleController::class);
    Route::apiResource('/vehicle-owners', VehicleOwnerController::class);
    Route::apiResource('/workshop-owners', WorkshopOwnerController::class);
    Route::apiResource('/workshops', WorkshopController::class);
    Route::apiResource('/calendar', CalendarController::class);
    Route::apiResource('/services', ServiceController::class);
    Route::any('/vehicles/api/index', [VehicleController::class, 'index_api']);
    Route::any('/vehicle-owners/api/index', [VehicleOwnerController::class, 'index_api']);
    Route::any('/workshop-owners/api/index', [WorkshopOwnerController::class, 'index_api']);
    Route::any('/workshops/api/index', [WorkshopController::class, 'index_api']);
    Route::any('/calendar/api/index/{is_approved}', [CalendarController::class, 'index_api']);
    Route::any('/services/api/index', [ServiceController::class, 'index_api']);
    Route::post('/vehicles/media', [VehicleController::class, 'storeMedia'])->name('vehicles.storeMedia');
    Route::post('/generate-vehicle', [VehicleController::class, 'generateVehicle']);
    Route::get('/change-password', [ChangePasswordController::class, 'show_form'])->name('change.password.form');
    Route::put('/change-password', [ChangePasswordController::class, 'change'])->name('change.password');
    Route::any('/delete-my-data', function () {
        return "<center><h1>Your data have been deleted successfully.</h1></center>";
    });
    Route::any('/vehicle_list/pdf', [VehicleController::class, 'listVehiclesInPdf']);
});

Route::post('/sanctum/token', function (Request $request) {
    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required',
    ]);

    $user = User::where('email', $request->email)->first();

    if (! $user || ! Hash::check($request->password, $user->password)) {
        throw ValidationException::withMessages([
            'email' => ['The provided credentials are incorrect.'],
        ]);
    }
    return $user->createToken($request->device_name)->plainTextToken;
});
Route::fallback(function () {
    return 'Silence is golden';
});
