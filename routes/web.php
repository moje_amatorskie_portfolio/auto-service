<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\WorkshopController;
use App\Http\Controllers\VehicleOwnerController;
use App\Http\Controllers\WorkshopOwnerController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\Auth\ChangePasswordController;
use App\Http\Controllers\Auth\SocialiteController;
use App\Http\Controllers\Auth\GoogleLoginController;
use App\Http\Controllers\MapController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;

Route::post('/sanctum/token', function (Request $request) {
    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required',
    ]);

    $user = User::where('email', $request->email)->first();

    if (! $user || ! Hash::check($request->password, $user->password)) {
        throw ValidationException::withMessages([
            'email' => ['The provided credentials are incorrect.'],
        ]);
    }

    return $user->createToken($request->device_name)->plainTextToken;
});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::resource('/vehicles', VehicleController::class);
    Route::resource('/vehicle-owners', VehicleOwnerController::class);
    Route::resource('/workshop-owners', WorkshopOwnerController::class);
    Route::resource('/workshops', WorkshopController::class);
    Route::resource('/calendar', CalendarController::class);
    Route::resource('/services', ServiceController::class);
    Route::get('/vehicles/api/index', [VehicleController::class, 'index_api']);
    Route::get('/vehicle-owners/api/index', [VehicleOwnerController::class, 'index_api']);
    Route::get('/workshop-owners/api/index', [WorkshopOwnerController::class, 'index_api']);
    Route::get('/workshops/api/index', [WorkshopController::class, 'index_api']);
    Route::any('/calendar/api/index/{is_approved}', [CalendarController::class, 'index_api']);
    Route::get('/services/api/index', [ServiceController::class, 'index_api']);
    Route::post('/vehicles/media', [VehicleController::class, 'storeMedia'])->name('vehicles.storeMedia');
    Route::post('/generate-vehicle', [VehicleController::class, 'generateVehicle']);
    Route::get('/change-password', [ChangePasswordController::class, 'show_form'])->name('change.password.form');
    Route::put('/change-password', [ChangePasswordController::class, 'change'])->name('change.password');
    Route::any('/delete-my-data', function () {
        return "<center><h1>Your data have been deleted successfully.</h1></center>";
    });
    Route::any('/vehicle_list/pdf', [VehicleController::class, 'listVehiclesInPdf']);
    Route::get('/map', [MapController::class, 'index'])->name('map.index');
    Route::get('/users', [UserController::class, 'index'])->name('users.index');
    Route::get('/users/api/index', [UserController::class, 'index_api']);
});

require __DIR__.'/auth.php';
Route::any('/login-with-google', [SocialiteController::class, 'loginWithGoogle'])->name('login-with-google');
Route::any('/callback-from-google', [SocialiteController::class, 'callbackFromGoogle'])->name('callback-from-google');
Route::any('/login-with-facebook', [SocialiteController::class, 'loginWithFacebook'])->name('login-with-facebook');
Route::any('/callback-from-facebook', [SocialiteController::class, 'callbackFromFacebook'])->name('callback-from-facebook');
Route::any('/choose-user-type-save', [SocialiteController::class, 'storeUserType'])->name('choose-user-type-save');
Route::any('/choose-user-type-form', [SocialiteController::class, 'showChooseUserTypeForm']);

Route::fallback(function () {
    return 'Silence is golden';
});
