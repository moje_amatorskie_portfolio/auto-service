const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.
js('resources/js/calendar/calendar.js', 'public/js').
js('resources/js/app.js', 'public/js').
js('resources/js/components/vehicles/sweetalert2_vehicles.js', 'public/js').
js('resources/js/components/vehicles/notyf_toasts.js', 'public/js').
js('resources/js/libraries/dropzone.js', 'public/js').
js('resources/js/libraries/splide.js', 'public/js').
js('resources/js/vehicles/datatables.js', 'public/js/vehicle_datatables.js').
js('resources/js/vehicle_owners/datatables.js', 'public/js/vehicle_owners_datatables.js').
js('resources/js/workshop_owners/datatables.js', 'public/js/workshop_owners_datatables.js').
js('resources/js/workshops/datatables.js', 'public/js/workshops_datatables.js').
js('resources/js/services/datatables.js', 'public/js/services_datatables.js').
js('resources/js/users/datatables.js', 'public/js/users_datatables.js').
postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]).
sass('resources/sass/app.scss', 'public/css').
sass('resources/sass/libraries/dropzone/dropzone.scss', 'public/css').
sass('resources/sass/libraries/splide.scss', 'public/css').
sass('resources/sass/vehicles/form.scss', 'public/css/vehicles').
version().
sourceMaps().
extract([]).
copy('resources/images/', 'public/images').
copy('resources/js/fullcalendar', 'public/js/fullcalendar').
copy('resources/css/fullcalendar', 'public/css/fullcalendar').
copy('resources/css/mapbox', 'public/css/mapbox');
mix.disableNotifications();