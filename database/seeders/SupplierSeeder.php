<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suppliers')->insert([
            'name' => 'Supplier A',
        ]);
        DB::table('suppliers')->insert([
            'name' => 'Supplier B',
        ]);
        DB::table('suppliers')->insert([
            'name' => 'Supplier C',
        ]);
        
        DB::table('supplier_workshop')->insert([
            'workshop_id' => 1,
            'supplier_id' => 1,
        ]);
        DB::table('supplier_workshop')->insert([
            'workshop_id' => 2,
            'supplier_id' => 2,
        ]);
        DB::table('supplier_workshop')->insert([
            'workshop_id' => 3,
            'supplier_id' => 3,
        ]);
    }
}
