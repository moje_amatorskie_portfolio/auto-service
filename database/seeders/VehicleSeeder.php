<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Services\VinGeneratorService;

class VehicleSeeder extends Seeder
{
    private $vinGeneratorService;

    public function __construct()
    {
        $this->vinGeneratorService = new VinGeneratorService();
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $makes = ['BMW', 'Fiat', 'Daewoo', 'VW', 'Ford', 'Jaguar', 'Star', 'Mini', 'Volga', 'Matsui', 'Skoda', 'Mitsubishi', 'Dacha', 'Jelcz','Honda', 'Ferrari', 'Acura', 'Alfa Romeo', 'Audi', 'Bentley', 'Cadillac', 'Chrysler', 'Ford', 'Hyundai', 'Jeep', 'Kia', 'Land Rover', 'Lexus', 'Lotus', 'Maserati', 'Mazda', 'Pontiac', 'Porsche', 'Subaru', 'Suzuki', 'Toyota', 'Volvo'];

        foreach ($makes as $make) {
            $this->seedVehicle($make);
        }
    }

    public function seedVehicle($make)
    {
        $model = Str::upper(Str::random(6));
        $fuel = rand(0, 1) ? 'petrol' : 'diesel';
        $capacity = rand(100, 200);
        $power = rand(200, 300);
        $vin =  $this->vinGeneratorService->vinGeneratorService();
        $plate = Str::upper(str_shuffle('DJT'))." ".rand(11111, 99999);
        $equipment = 'equipment';
        $description = 'description';
        $vehicleId = DB::table('vehicles')->insertGetId([
            'make' => $make,
            'model' => $model,
            'fuel' => $fuel,
            'capacity' => $capacity,
            'power' => $power,
            'vin' => $vin,
            'plate' => $plate,
            'equipment' => $equipment,
            'description' => $description
        ]);
        $this->seedUserVehicle($vehicleId);
    }

    public function seedUserVehicle($vehicleId)
    {
        DB::table('user_vehicle')->insert([
            'user_id' => rand(2, 7),
            'vehicle_id' => $vehicleId,
        ]);
        $this->seedVehicleWorkshop($vehicleId);
    }

    public function seedVehicleWorkshop($vehicleId)
    {
        DB::table('vehicle_workshop')->insert([
            'vehicle_id' => $vehicleId,
            'workshop_id' => rand(1, 5),
        ]);
    }
}
