<?php

namespace Database\Seeders;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use App\Models\User;

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $guestRole = Role::create(['name' => 'guest role']);
        $vehicleOwnerRole = Role::create(['name' => 'vehicle owner']);
        $workshopOwnerRole = Role::create(['name' => 'workshop owner']);
        $superAdminRole = Role::create(['name' => 'super admin']);

        $permissionArrayForAllRoles = [
            'show dashboard',
            'index vehicles',
            'index calendar',
            'index vehicle history',
            'index services',
            'show service',
            'index map',
        ];

        $permissionArrayForVehicleOwners = [
            'index vehicles',
            'show vehicle',
            'create vehicle',
            'store vehicle',
            'edit vehicle',
            'update vehicle',
            'destroy vehicle',
            'create service',
            'store service',
            'edit service',
            'update service',
            'destroy service'
        ];

        $permissionArrayForWorkshopOwners = [
            'index vehicles',
            'show vehicle',
            'destroy vehicle',
            'index vehicle owners',
            'show vehicle owner',
            'edit vehicle owner',
            'update vehicle owner',
            'destroy vehicle owner',
            'create service',
            'store service',
            'edit service',
            'update service',
            'destroy service'
        ];

        $permissionArrayForGuest = [
            'create vehicle',
            'store vehicle',
            'show vehicle'
        ];

        $this->createAndGivePermissionsToRoles($permissionArrayForAllRoles, [$vehicleOwnerRole, $workshopOwnerRole, $guestRole]);
        $this->createAndGivePermissionsToRoles($permissionArrayForVehicleOwners, [$vehicleOwnerRole]);
        $this->createAndGivePermissionsToRoles($permissionArrayForWorkshopOwners, [$workshopOwnerRole]);
        $this->createAndGivePermissionsToRoles($permissionArrayForGuest, [$guestRole]);

        $guest = User::where('email', 'guest@guest.com')->firstOrFail();
        $guest->assignRole($guestRole);
        $vehicleOwner = User::where('email', 'vehicle.owner@vehicle.owner.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);
        $vehicleOwner = User::where('email', 'vehicle.owner1@vehicle.owner.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);
        $vehicleOwner = User::where('email', 'vehicle.owner2@vehicle.owner.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);
        $vehicleOwner = User::where('email', 'vehicle.owner3@vehicle.owner.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);
        $vehicleOwner = User::where('email', 'vehicle.owner4@vehicle.owner.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);
        $vehicleOwner = User::where('email', 'vehicle.owner5@vehicle.owner.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);
        $vehicleOwner = User::where('email', 'jan.kowalski@jan.kowalski.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);
        $vehicleOwner = User::where('email', 'marcin.nowak@marcin.nowak.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);
        $vehicleOwner = User::where('email', 'marek.kowalczyk@marek.kowalczyk.com')->firstOrFail();
        $vehicleOwner->assignRole($vehicleOwnerRole);

        $workshopOwner = User::where('email', 'workshop.owner@workshop.owner.com')->firstOrFail();
        $workshopOwner->assignRole($workshopOwnerRole);
        $workshopOwner = User::where('email', 'workshop.owner1@workshop.owner.com')->firstOrFail();
        $workshopOwner->assignRole($workshopOwnerRole);
        $workshopOwner = User::where('email', 'workshop.owner2@workshop.owner.com')->firstOrFail();
        $workshopOwner->assignRole($workshopOwnerRole);
        $workshopOwner = User::where('email', 'workshop.owner3@workshop.owner.com')->firstOrFail();
        $workshopOwner->assignRole($workshopOwnerRole);
        $workshopOwner = User::where('email', 'workshop.owner_x@workshop.owner.com')->firstOrFail();
        $workshopOwner->assignRole($workshopOwnerRole);
        $workshopOwner = User::where('email', 'workshop.owner_y@workshop.owner.com')->firstOrFail();
        $workshopOwner->assignRole($workshopOwnerRole);

        $superAdmin = User::where('email', 'superadmin@superadmin.com')->firstOrFail();
        $superAdmin->assignRole($superAdminRole);
    }

    private function createAndGivePermissionsToRoles($permission, $roles)
    {
        foreach ($permission as $permission) {
            $perm = Permission::firstOrCreate(['name' => $permission]);
            foreach ($roles as $role) {
                $role->givePermissionTo($perm);
            }
        }
    }
}
