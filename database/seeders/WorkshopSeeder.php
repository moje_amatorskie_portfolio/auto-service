<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkshopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('workshops')->insert([
            'name' => 'Workshop A',
            'place' => 'Warsaw, Masovian Voivodeship, Poland',
            'longitude' => '21.00879',
            'latitude' => '52.231475',
            'created_at' => now(),
        ]);
        DB::table('workshops')->insert([
            'name' => 'Workshop B',
            'place' => 'Toruń, Kuyavian-Pomeranian Voivodeship, Poland',
            'longitude' => '18.604809',
            'latitude' => '53.010272',
            'created_at' => now(),
        ]);
        DB::table('workshops')->insert([
            'name' => 'Workshop C',
            'place' => 'Bydgoszcz, Kuyavian-Pomeranian Voivodeship, Poland',
            'longitude' => '18.000253',
            'latitude' => '53.121965',
            'created_at' => now(),
        ]);
        DB::table('workshops')->insert([
            'name' => 'Workshop D',
            'place' => 'Lublin, Lublin Voivodeship, Poland',
            'longitude' => '22.570102',
            'latitude' => '51.250559',
            'created_at' => now(),
        ]);
        DB::table('workshops')->insert([
            'name' => 'Workshop X',
            'place' => 'Wrocław, Lower Silesian Voivodeship, Poland',
            'longitude' => '17.032669',
            'latitude' => '51.108978',
            'created_at' => now(),
        ]);
        DB::table('workshops')->insert([
            'name' => 'Workshop Y',
            'place' => 'Gdańsk, Pomeranian Voivodeship, Poland',
            'longitude' => '18.654023',
            'latitude' => '54.348291',
            'created_at' => now(),
        ]);
        DB::table('workshops')->insert([
            'name' => 'none',
            'place' => 'none',
            'longitude' => '',
            'latitude' => '',
            'created_at' => now(),
        ]);
    }
}
