<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use DateTime;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $this->addUser(collect([
            'first_name' => 'Gość',
            'last_name' => 'Nowak',
            'email' => 'guest@guest.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-02-23 12:13:12'
        ]));

        $this->addUser(collect([
            'first_name' => 'Vehicle',
            'last_name' => 'Owner',
            'email' => 'vehicle.owner@vehicle.owner.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-02-25 12:13:12'
        ]));

        $this->addUser(collect([
            'first_name' => 'Vehicle',
            'last_name' => 'Owner1',
            'email' => 'vehicle.owner1@vehicle.owner.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-02-23 12:16:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Vehicle',
            'last_name' => 'Owner2',
            'email' => 'vehicle.owner2@vehicle.owner.com',
            'password' => Hash::make('password'),
            'created_at' => '1992-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Vehicle',
            'last_name' => 'Owner3',
            'email' => 'vehicle.owner3@vehicle.owner.com',
            'password' => Hash::make('password'),
            'created_at' => '1970-02-23 14:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Vehicle',
            'last_name' => 'Owner4',
            'email' => 'vehicle.owner4@vehicle.owner.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-04-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Vehicle',
            'last_name' => 'Owner5',
            'email' => 'vehicle.owner5@vehicle.owner.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Jan',
            'last_name' => 'Kowalski',
            'email' => 'jan.kowalski@jan.kowalski.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Marcin',
            'last_name' => 'Nowak',
            'email' => 'marcin.nowak@marcin.nowak.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Marek',
            'last_name' => 'Kowalczyk',
            'email' => 'marek.kowalczyk@marek.kowalczyk.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Workshop',
            'last_name' => 'Owner',
            'email' => 'workshop.owner@workshop.owner.com',
            'password' => Hash::make('password'),
            'workshop_id' => 1, //this is only for workshop_owners
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Workshop',
            'last_name' => 'Owner1',
            'email' => 'workshop.owner1@workshop.owner.com',
            'password' => Hash::make('password'),
            'workshop_id' => 2,
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Workshop',
            'last_name' => 'Owner2',
            'email' => 'workshop.owner2@workshop.owner.com',
            'password' => Hash::make('password'),
            'workshop_id' => 3,
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Workshop',
            'last_name' => 'Owner3',
            'email' => 'workshop.owner3@workshop.owner.com',
            'password' => Hash::make('password'),
            'workshop_id' => 4,
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'X_first_name',
            'last_name' => 'X_last_name',
            'email' => 'workshop.owner_x@workshop.owner.com',
            'password' => Hash::make('password'),
            'workshop_id' => 5,
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Y_first_name',
            'last_name' => 'Y_last_name',
            'email' => 'workshop.owner_y@workshop.owner.com',
            'password' => Hash::make('password'),
            'workshop_id' => 6,
            'created_at' => '1998-02-23 12:13:12'
        ]));
        $this->addUser(collect([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'superadmin@superadmin.com',
            'password' => Hash::make('password'),
            'created_at' => '1998-02-23 12:13:12'
        ]));
    }
    public function addUser($userDataCollection)
    {
        $user = new User();
        $user->first_name = $userDataCollection->get('first_name');
        $user->last_name = $userDataCollection->get('last_name');
        $user->email = $userDataCollection->get('email');
        $user->address = 'Address';
        $user->password = $userDataCollection->get('password');
        if ($userDataCollection->has('workshop_id')) {
            $user->workshop_id = $userDataCollection->get('workshop_id');
        };
        $user->save();
    }
}
