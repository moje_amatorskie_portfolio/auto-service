<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('title', 50);
            $table->string('description', 222);
            $table->dateTime('service_day');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->unsignedBigInteger('vehicle_id');
            $table->unsignedBigInteger('workshop_id');
            $table->tinyInteger('is_approved')->default(0);
            $table->foreign('vehicle_id')
                ->references('id')->on('vehicles')
                ->onDelete('cascade');
            $table->foreign('workshop_id')
                ->references('id')->on('workshops')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
